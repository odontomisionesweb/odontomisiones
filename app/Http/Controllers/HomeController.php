<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Pais;
use App\Provincia;
use App\Ciudad;
use App\Barrio;
use App\Proveedor;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(auth()->user()->rol_id==4){
            return redirect()->route('auditorias.index');
        }
        $u = Auth::user();
        $cantPaises=Pais::count();
        $cantProvincias=Provincia::count();
        $cantCiudades=Ciudad::count();
        $cantBarrios=Barrio::count();
        $cantProveedores=Proveedor::count();
        $cantUserAdm=User::whereRol_id(1)->count();
        $cantUserOpe=User::whereRol_id(2)->count();
        $cantUserTec=User::whereRol_id(3)->count();
        //dd($cantUserTec);
        return view('home', compact('u', 'cantPaises', 'cantProvincias', 'cantCiudades', 'cantBarrios', 'cantProveedores', 'cantUserAdm', 'cantUserOpe', 'cantUserTec'));
    }
}
