<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->string('apellido');
            $table->string('telefono');
            $table->string('notas_part');

            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');  //  Pais   ,  Provincia, Ciudad,  Barrio,     campo"domicilio"
            $table->string('domicilio'); //Argentina, Misiones, Posadas, Villa Poujade, Calle 45B 8338
            $table->unsignedBigInteger('barrio_id');
            $table->unsignedBigInteger('rol_id');
            //$table->unsignedBigInteger('estado_crediticio_id');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
