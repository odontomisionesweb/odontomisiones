<?php

use Illuminate\Database\Seeder;

class BarrioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('barrios')->insert([
            'codigo'=> 'BCHCR3233',
        'nombre'=> 'Chacra 32-33',
        'ciudad_id'=>1,

        ]);

        DB::table('barrios')->insert([
            'codigo'=> 'BVS',
        'nombre'=> 'Villa Sarita',
        'ciudad_id'=>1,

        ]);

        DB::table('barrios')->insert([
            'codigo'=> 'BA4',
        'nombre'=> 'A4',
        'ciudad_id'=>1,

        ]);

        DB::table('barrios')->insert([
            'codigo'=> 'BCH33',
            'nombre'=> 'Chacra 32-33',
            'ciudad_id'=>2,

        ]);
    }

}
