<?php

use Illuminate\Database\Seeder;

class CiudadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ciudades')->insert([
            'codigo'=> 'POS',
        'nombre'=> 'Posadas',
        'cod_postal'=>'3301',
        'provincia_id'=>1,

        ]);

        DB::table('ciudades')->insert([
            'codigo'=> 'APO',
        'nombre'=> 'Apostoles',
        'cod_postal'=>'3305',
        'provincia_id'=>1,

        ]);

        DB::table('ciudades')->insert([
            'codigo'=> 'SJO',
        'nombre'=> 'San Jose',
        'cod_postal'=>'3306',
        'provincia_id'=>1,

        ]);
    }

}
