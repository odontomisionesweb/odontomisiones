<?php

use Illuminate\Database\Seeder;

class IncidenciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

     /*
        Usuario (tecnico_id)
        Id  Nombre      Rol
        1   Jorge       Tecnico
        2   Lisandro    Tecnico
        3   Emiliano    Tecnico

        Usuario (cliente_id)
        Id  Nombre      Rol
        4   Cliente1    Cliente
        5   Cliente2    Cliente
        6   Cliente3    Cliente

     */
    public function run()
    {
        DB::table('incidencias')->insert([
        'descripcion'=> 'Incidencia 1',
        'nota'=> 'Nota Inc1',
        'prioridad_id'=>1,
        'estado_actual_id'=>1,
        'tecnico_id'=>1,
        'cliente_id'=>4,
        'equipo_id'=>1,
        'created_at'=>'1999-04-06',

        ]);

        DB::table('incidencias')->insert([
        'descripcion'=> 'Incidencia 2',
        'nota'=> 'Nota Inc1',
        'prioridad_id'=>2,
        'estado_actual_id'=>1,
        'tecnico_id'=>2,
        'cliente_id'=>5,
        'equipo_id'=>2,
        'created_at'=>'2006-04-06',

        ]);

        DB::table('incidencias')->insert([
        'descripcion'=> 'Incidencia 3',
        'nota'=> 'Nota Inc3',
        'prioridad_id'=>1,
        'estado_actual_id'=>1,
        'tecnico_id'=>3,
        'cliente_id'=>8,
        'equipo_id'=>3,
        'created_at'=>'2009-02-12',

        ]);
    }



}
