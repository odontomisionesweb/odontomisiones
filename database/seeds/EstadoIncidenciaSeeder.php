<?php

use Illuminate\Database\Seeder;

class EstadoIncidenciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('estados_incidencias')->insert([
        'nombre'=> 'Creado',
        ]);

        DB::table('estados_incidencias')->insert([
        'nombre'=> 'Terminado',
        ]);

        DB::table('estados_incidencias')->insert([
        'nombre'=> 'Cancelado',
        ]);
    }
}
