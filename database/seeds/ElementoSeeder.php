<?php

use Illuminate\Database\Seeder;

class ElementoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('elementos')->insert([
        'descripcion'=> 'Foco Odontologico Halogeno',
        'foto'=>'Foto Foco',
        'stock'=>20,
        'stock_min'=>5,
        'stock_max'=>50,
        'es_pedido'=>false,
        'paquete'=>'Focos',
        'cantidad_paquete'=>10,
        ]);

        DB::table('elementos')->insert([
        'descripcion'=> 'Apoya Brazo Universal Central Rebatible Regulable Negro',
        'foto'=>'Foto apoyabrazo',
        'stock'=>30,
        'stock_min'=>15,
        'stock_max'=>60,
        'es_pedido'=>false,
        'paquete'=>'Apoyabrazos',
        'cantidad_paquete'=>20,
        ]);

        DB::table('elementos')->insert([
        'descripcion'=> 'Pedal de control para odontología',
        'foto'=>'Foto Mesa',
        'stock'=>2,
        'stock_min'=>1,
        'stock_max'=>5,
        'es_pedido'=>false,
        'paquete'=>'Pedales',
        'cantidad_paquete'=>1,
        ]);
    }
}



