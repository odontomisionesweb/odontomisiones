<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class User extends Authenticatable implements Auditable{
    use \OwenIt\Auditing\Auditable;
    use Notifiable;

    public $timestamps = true;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
        'apellido',
        'email',
        'password',
        'domicilio',
        'barrio_id',
        'rol_id',
        'telefono',
        'notas_part',

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function barrio(){
        return $this->belongsTo('App\Barrio');
     }

     public function direccion(){
         return $this->domicilio . ", " . $this->barrio->nombre . ", " . $this->barrio->ciudad->nombre . ", " . $this->barrio->ciudad->provincia->nombre . ", " . $this->barrio->ciudad->provincia->pais->nombre;
     }

    public function rol(){
        return $this->belongsTo('App\Rol');
    }

    public function nombre_completo(){
        return $this->nombre . " " . $this->apellido;
    }


    public function estado_crediticio(){
        return $this->belongsTo('App\EstadoCrediticio');
     }

    public function existe_referencia()
    {

        $ref1 = Incidencia::whereCliente_id($this->id)->count();
        $ref2 = Incidencia::whereTecnico_id($this->id)->count();

        if($ref1 > 0 || $ref2 > 0) //existe referencia
        {
            return true;
        }
        else    //no hay referencia
        {
            return false;
        }

    }

}
