<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Repuesto;

class RepuestoController extends Controller
{
    public function index()
    {
        $repuestos=Repuesto::orderBy('descripcion')->get();
        return view('repuestos.index',compact('repuestos'));
    }

    public function create()
    {
        return view('repuestos.create');
    }

    public function store(Request $request, Repuesto $repuesto)
    {
        $request->validate([
            'descripcion'=>[ Rule::unique('repuestos'),
                        'required', 'regex:/^[a-zA-Z]+$/u', 'max:5' ],

            //'codigo' => 'unique:paises|required|max:5|regex:/^[a-zA-Z]+$/u',//regex sin espacios
            'stock'=>'required'
        ], [
            'required' => 'Campo requerido',

        ]);

        $repuesto=Repuesto::create([
            'descripcion'=>$request->descripcion,
            'stock'=>$request->stock,
        ]);

        return redirect()->route('repuestos.index')->withMessage('Repuesto creado');
    }

    public function edit(Repuesto $repuesto)
    {
        return view('repuestos.edit',compact('repuesto'));
    }

    public function update(Request $request, Rpuesto $repuesto)
    {

        $repuesto->update([
            'descripcion' => $request->descripcion,
            'stock' =>$request->stock,
        ]);

        return redirect()->route('repeustos.index')->withMessage('Repuesto modificado');
    }

    public function destroy(Repuesto $repuesto) {



        $repuesto->delete();
        return redirect()->route('repuestos.index')->with('message','Repuesto eliminado');




    }
}
