<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incidencia extends Modelz
{
    public $timestamps = true;

    protected $fillable = ['descripcion', 'nota', 'prioridad_id', 'estado_actual_id', 'tecnico_id', 'cliente_id', 'equipo_id'];

    protected $table = 'incidencias';

    public function prioridad(){
        return $this->belongsTo('App\Prioridad');
    }

    public function estado_incidencia(){
        return $this->belongsTo('App\EstadoIncidencia');
    }

    public function tecnico(){
        return $this->belongsTo('App\User', 'tecnico_id');
    }

    public function cliente(){
        return $this->belongsTo('App\User', 'cliente_id');
    }

    public function equipo(){
        return $this->belongsTo('App\Equipo');
    }

    public function consumos(){
        return $this->hasMany('App\Consumo');
    }

    public function historial_estado(){
        return $this->hasMany('App\HistorialEstado');
    }
}
