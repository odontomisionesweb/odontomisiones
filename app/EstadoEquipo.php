<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoEquipo extends Model
{
    public $timestamps = false;

    protected $fillable = ['nombre'];

    protected $table = 'estado_equipos';

}
