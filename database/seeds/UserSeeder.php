<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        DB::table('users')->insert([
        'nombre'=>'Jorge',
        'notas_part'=>'es una buena persona',
        'apellido'=>'Rodriguez',
        'email'=> 'jorge@mail.com',
        'password'=> bcrypt(12345678),
        'domicilio'=>'9 de Julio',
        'barrio_id'=>4,
        'rol_id'=>3, //Tecnico
        'created_at'=>'2020-07-01',
        'telefono'=>'44574632',

        ]);

        DB::table('users')->insert([
        'nombre'=>'Lisandro',
        'notas_part'=>'es una buena persona',
        'apellido'=>'Villalba',
        'email'=> 'lisandro@mail.com',
        'password'=> bcrypt(12345678),
        'domicilio'=>'cerca de la chevrolet',
        'barrio_id'=>4,
        'rol_id'=>3, //Tecnico
        'created_at'=>'2020-07-02',
        'telefono'=>'44574632',

        ]);

        DB::table('users')->insert([
        'nombre'=>'Emiliano',
        'notas_part'=>'es una buena persona',
        'apellido'=>'Pereyra',
        'email'=> 'emiliano@mail.com',
        'password'=> bcrypt(12345678),
        'domicilio'=>'en Posadas',
        'barrio_id'=>2,
        'rol_id'=>3, //Tecnico
        'created_at'=>'2020-07-06',
        'telefono'=>'44574632',

        ]);

        DB::table('users')->insert([
        'nombre'=>'Cliente1',
        'notas_part'=>'es una buena persona',
        'apellido'=>'Cliente',
        'email'=> 'cli1@mail.com',
        'password'=> bcrypt(12345678),
        'domicilio'=>'no se',
        'barrio_id'=>1,
        'rol_id'=>4, //Administrador
        'created_at'=>'2020-07-08',
        'telefono'=>'44574632',

        ]);

        DB::table('users')->insert([
        'nombre'=>'Cliente2',
        'notas_part'=>'es una buena persona',
        'apellido'=>'Cliente2',
        'email'=> 'cli2@mail.com',
        'password'=> bcrypt(12345678),
        'domicilio'=>'no se',
        'barrio_id'=>1,
        'rol_id'=>4, //Administrador
        'created_at'=>'2020-07-14',
        'telefono'=>'44574632',

        ]);

        DB::table('users')->insert([
        'nombre'=>'Admin',
        'notas_part'=>'es una buena persona',
        'apellido'=>'Administrador',
        'email'=> 'adm@mail.com',
        'password'=> bcrypt(12345678),
        'domicilio'=>'Villa 31',
        'barrio_id'=>1,
        'rol_id'=>1, //Administrador
        'created_at'=>'2020-07-20',
        'telefono'=>'44574632',

        ]);

        DB::table('users')->insert([
        'nombre'=>'Operadora',
        'notas_part'=>'es una buena persona',
        'apellido'=>'Operadora',
        'email'=> 'ope@mail.com',
        'password'=> bcrypt(12345678),
        'domicilio'=>'no se',
        'barrio_id'=>1,
        'rol_id'=>2, //Operador
        'created_at'=>'2020-07-19',
        'telefono'=>'44574632',


        ]);

        DB::table('users')->insert([
            'nombre'=>'Laura',
            'notas_part'=>'es una buena persona',
            'apellido'=>'Daruski',
            'email'=> 'lauduski@mail.com',
            'password'=> bcrypt(12345678),
            'domicilio'=>'Calle Blas Pareda',
            'barrio_id'=>1,
            'rol_id'=>4, //Auditor
            'created_at'=>'2020-07-15',
            'telefono'=>'44574632',


            ]);
    }
}
