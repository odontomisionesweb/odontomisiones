<?php

namespace App\Http\Controllers;
use App\Provincia;
use App\Pais;
use Illuminate\Validation\Rule;
use App\Http\Requests\CiudadRequest;


use App\Ciudad;
use Illuminate\Http\Request;

class CiudadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $provincias=Provincia::all();

        if(count($request->all())>1)
        {
            //dd($request->all());
            $sql = Ciudad::select('ciudades.*');

            if($request->provincia_id)
            {
                $sql = $sql->whereProvincia_id($request->provincia_id);
            }


            $ciudades=$sql->orderBy('nombre','asc')->get();
            $provincia_id=$request->provincia_id;


        }
        else
        {
            $provincia_id=null;

            $ciudades=Ciudad::orderBy('nombre','asc')->get();


        }

        return view('ciudades.index',[
            "ciudades"         =>  $ciudades,
            "provincia_id"        =>  $provincia_id,
            "provincias"         =>  $provincias,

            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $provincias=Provincia::all();
        return view('ciudades.create', compact('provincias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request, Ciudad $ciudades)
    {
        $request->validate([
            'codigo'=>[ Rule::unique('ciudades')->ignore($ciudades),
                        'required', 'regex:/^[a-zA-Z]+$/u', 'max:5' ],

            //'codigo' => 'unique:paises|required|max:5|regex:/^[a-zA-Z]+$/u',//regex sin espacios
            //VALIDACION QUE FUNCIONA CON LOS ACENTOS Y NUMEROS
            'nombre'=>'required|regex:/^[0-9-A-Za-zñÑáéíóúÁÉÍÓÚ][0-9-A-Za-zñÑáéíóúÁÉÍÓÚ ]+$/u|max:50',//regex con espacios
            'cod_postal'=>[ Rule::unique('ciudades')->ignore($ciudades),
                        'required', 'regex:/^[0-9]+$/u', 'max:5' ],
            'provincia_id'=>'required',

        ], [
            'required' => 'Campo requerido',
            'codigo.unique' => 'Está ingresando un código existente',
            'regex' => 'Complete el campo únicamente con letras',
            'cod_postal.regex' => 'Complete el campo únicamente con numeros',
            'cod_postal.unique' => 'Está ingresando un código existente',
            //no toma
            'codigo.max' => 'Se permiten hasta 5 caracteres',
            'nombre.max' => 'Se permiten hasta 50 caracteres',
        ]);
        $ciudades=Ciudad::create([
            'codigo'=>$request->codigo,
            'nombre'=>$request->nombre,
            'cod_postal'=>$request->cod_postal,
            'provincia_id'=>$request->provincia_id,
        ]);

        return redirect()->route('ciudades.index')->withMessage('Ciudad creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Ciudad $ciudad)
    {
        $provincias=Provincia::all();
        return view('ciudades.edit',compact('ciudad','provincias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ciudad $ciudad)
    {
        $request->validate([
            'codigo'=>[ Rule::unique('ciudades')->ignore($ciudad),
                        'required', 'regex:/^[a-zA-Z]+$/u', 'max:5' ],

            'cod_postal'=>[ Rule::unique('ciudades')->ignore($ciudad),
                        'required', 'regex:/^[0-9]+$/u', 'max:5' ],

            //'codigo' => 'unique:paises|required|max:5|regex:/^[a-zA-Z]+$/u',//regex sin espacios
            'nombre'=>'required|regex:/^[a-zA-Z][a-zA-Z ]+$/u|max:255',//regex con espacios
        ], [
            'required' => 'Campo requerido',
            'codigo.unique' => 'Está ingresando un código existente',
            'regex' => 'Complete el campo unicamente con letras',
            'cod_postal.regex' => 'Complete el campo unicamente con numeros',
            
            //no toma
            'max' => 'Excede al maximo de caracteres',
        ]);
        $ciudad->update([
            'codigo'=>$request->codigo,
            'nombre' =>$request->nombre,
            'cod_postal'=>$request->cod_postal,
        ]);

        return redirect()->route('ciudades.index')->withMessage('Ciudad modificada');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ciudad $ciudad)
    {

        //dd("destroy");

        if ($ciudad->existe_referencia()) {
            return redirect()->route('ciudades.index')->withErrors('No se puede eliminar la Ciudad, hay referencias asociadas');
        }

        $ciudad->delete();
        return redirect()->route('ciudades.index')->with('message','Ciudad eliminada');


    }
}
