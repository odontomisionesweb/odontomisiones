{!! Form::model(Request::only(
        ['provincia_id']),
        ['url' => 'ciudades/', 'method'=>'GET', 'autocomplete'=>'on', 'role'=>'search']

        )!!}

        <div class="form-group row">
            <label for="provincia_id" class="col-md-4 col-form-label text-md-right">Provincia</label>
            <div class="col-md-6">
                <select
                    name="provincia_id"
                    id="provincia_id"
                    class="provincia_id custom-select"
                    >
                    @foreach ($provincias as $provincia)
                        <option
                            value="{{$provincia->id}}"
                            @if($provincia_id!=null && $provincia_id==$provincia->id)
                                selected
                            @endif
                        >
                        {{$provincia->nombre}}
                        </option>
                    @endforeach
                    <option
                        value="0"
                        @if($provincia_id == null || $provincia_id==0)
                            selected
                        @endif
                    >
                        -- Todas las provincias --
                    </option>
                </select>
            </div>

        </div>


        <div class="form-group row">
            <label for="buscar" class="col-md-4 col-form-label text-md-right"></label>
            <div class="col-md-6">
                <span class="input-group-btn">
                    <button
                        title="buscar"
                        type="submit"
                        id="bt_add"
                        name="filtrar"
                        class="btn btn-primary btn-responsive">
                            <i class="fa fa-filter"></i> Filtrar
                    </button>

                    <a

                    href= "{{ route('ciudades.index') }}"
                    class="btn btn-light"
                    >
                    <i class="fas fa-eraser"></i>
                        ... Limpiar
                </a>

                </span>
            </div>
        </div>






    {{Form::close()}}


