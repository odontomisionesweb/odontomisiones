<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoCrediticio extends Model
{
    public $timestamps = true;

    protected $table = 'estado_crediticios';

    protected $fillable = ['nombre'];

    public function users(){
        return $this->belongsTo('App\User');
     }
}
