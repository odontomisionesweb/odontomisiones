<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Proveedor;

use App\Pais;
use App\Provincia;
use App\Ciudad;
use App\Barrio;

use Illuminate\Validation\Rule;

class ProveedorController extends Controller
{

    public function buscarProvincia(Request $request)
    {
        $provincias=Provincia::select('nombre','id')
            ->where('pais_id',$request->id)
            ->get();
        return response()->json($provincias);
    }

    public function buscarCiudad(Request $request)
    {

        $ciudades=Ciudad::select('nombre','id')
            ->where('provincia_id',$request->id)
            ->get();
        return response()->json($ciudades);

    }

    public function buscarBarrio(Request $request)
    {

        $barrios=Barrio::select('nombre','id')
            ->where('ciudad_id',$request->id)
            ->get();
        return response()->json($barrios);

    }
    
    public function index()
    {
        $proveedores=Proveedor::orderBy('razon_social')->get();
        return view('proveedores.index',compact('proveedores'));
    }

    public function create()
    {
    	$paises=Pais::all();
        $provincias=Provincia::all();
        $ciudades=Ciudad::all();
        $barrios=Barrio::all();
        return view('proveedores.create',compact('paises','provincias','ciudades', 'barrios'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'email'=>[ Rule::unique('proveedores'),
                        'required', 'max:50', 'email' ],


            'razon_social'=>'required|regex:/^[a-zA-Z][a-zA-Z ]+$/u|max:50',
            'descripcion'=>'required|regex:/^[a-zA-Z][a-zA-Z ]+$/u|max:100',
            'telefono'=>'required|numeric',//regex con espacios
        ], [
            'required' => 'Campo requerido',
            'codigo.unique' => 'Está ingresando un código existente',
            'regex' => 'Complete el campo únicamente con letras',
            //no toma
            'codigo.max' => 'Se permiten hasta 5 caracteres',
            'nombre.max' => 'Se permiten hasta 50 caracteres',
            'numeric' => 'Se permiten valores numéricos',
            'email'=>'Mail No Válido (ejemplo@mail.com)',
        ]);

        $proveedor=Proveedor::create([
            'razon_social'      =>    $request->razon_social,
            'descripcion'        =>    $request->descripcion,
            'telefono'    =>    $request->telefono,
            'email'       =>    $request->email,
            'domicilio'   =>    $request->domicilio,
            'barrio_id'   =>    $request->barrio_id,
        ]);

        return redirect()->route('proveedores.index')->withMessage('Proveedor creado');
    }

    public function edit(Proveedor $proveedor)
    {
        $paises=Pais::all();
        $provincias=Provincia::all();
        $ciudades=Ciudad::all();
        $barrios=Barrio::all();
        return view('proveedores.edit',compact('proveedor', 'paises', 'provincias', 'ciudades', 'barrios'));

    }


    public function update(Request $request, Proveedor $proveedor)
    {

        $request->validate([
            'email'=>[ Rule::unique('proveedores')->ignore($proveedor),
                        'required', 'max:50', 'email' ],


            'razon_social'=>'required|regex:/^[a-zA-Z][a-zA-Z ]+$/u|max:50',
            'descripcion'=>'required|regex:/^[a-zA-Z][a-zA-Z ]+$/u|max:100',
            'telefono'=>'required|numeric',//regex con espacios
        ], [
            'required' => 'Campo requerido',
            'codigo.unique' => 'Está ingresando un código existente',
            'regex' => 'Complete el campo únicamente con letras',
            //no toma
            'codigo.max' => 'Se permiten hasta 5 caracteres',
            'nombre.max' => 'Se permiten hasta 50 caracteres',
            'numeric' => 'Se permiten valores numéricos',
            'email'=>'Mail No Válido (ejemplo@mail.com)',
        ]);

        $proveedor->update([
            'razon_social'      =>    $request->razon_social,
            'descripcion'        =>    $request->descripcion,
            'telefono'    =>    $request->telefono,
            'email'       =>    $request->email,
            'domicilio'   =>    $request->domicilio,
            'barrio_id'   =>    $request->barrio_id,
        ]);

        return redirect()->route('proveedores.index')->withMessage('Proveedor modificado');

    }

    public function destroy(Proveedor $proveedor) {

        $proveedor->delete();
        return redirect()->route('proveedores.index')->with('message','Proveedor eliminado');




    }

}
