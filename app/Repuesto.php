<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Repuesto extends Model
{
    protected $fillable = [
        'descripcion',
        'foto',
        'stock',
        'stock_min',
        'stock_max',
        'es_pedido',
        'paquete',
        'cantidad_paquete',
    ];

    public $timestamps = false;

    protected $table = 'elementos';
}
