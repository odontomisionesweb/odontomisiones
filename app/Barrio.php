<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Barrio extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $fillable = [
        'codigo','nombre', 'ciudad_id',
    ];

    public $timestamps = false;

    protected $table = 'barrios';

    public function ciudad(){
        return $this->belongsTo('App\Ciudad');
    }

    public function direccion(){
        return $this->ciudad->nombre;
    }

    public function existe_referencia()
    {

        $ref = User::whereBarrio_id($this->id)->count();
        //dd($ref);

        if($ref > 0) //existe referencia
        {
            return true;
        }
        else    //no hay referencia
        {
            return false;
        }

    }
}
