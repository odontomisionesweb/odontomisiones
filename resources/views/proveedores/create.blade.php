@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">

            <div class="card">
                <div class="card-header h3">
                    Crear Proveedor
                    <a href="{{route('proveedores.index')}}">
                    <button title="volver" class="btn btn-secondary btn-responsive">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Atrás
                    </button>
                </a>
                </div>


                <div class="container card-body">
                    <form method="POST" action="{{ route('proveedores.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="razon_social" class="col-md-4 col-form-label text-md-right">Razón Social</label>
                            <div class="col-md-6">
                                <input id="razon_social" type="string" class="form-control @error('razon_social') is-invalid @enderror" name="razon_social" value="{{ old('razon_social') }}" required autocomplete="razon_social" autofocus>
                                @error('razon_social')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="descripcion" class="col-md-4 col-form-label text-md-right">Descripción</label>
                            <div class="col-md-6">
                                <input id="descripcion" type="string" class="form-control @error('descripcion') is-invalid @enderror" name="descripcion" value="{{ old('descripcion') }}" required autocomplete="descripcion" autofocus>
                                @error('descripcion')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="telefono" class="col-md-4 col-form-label text-md-right">Teléfono</label>
                            <div class="col-md-6">
                                <input id="telefono" type="string" class="form-control @error('telefono') is-invalid @enderror" name="telefono" value="{{ old('telefono') }}" autocomplete="telefono" autofocus>
                                @error('telefono')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">E-mail</label>
                            <div class="col-md-6">
                                <input id="email" type="string" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>



                        <div class="col-md-12">
                            <label for="pais_id" class="col-md-4 col-form-label text-md-right">País</label>
                                <select
                                    name="pais_id"
                                    id="pais_id"
                                    class="pais_id custom-select"
                                    required
                                    style="width:50%"
                                    >
                                    <option
                                        value="0"
                                        disabled="true"
                                        selected="true"
                                        title="Seleccione un pais"
                                        >
                                        -Seleccione un pais-
                                    </option>
                                    @foreach ($paises as $p)
                                        <option
                                            value="{{$p->id }}">
                                                {{$p->nombre}}
                                        </option>
                                    @endforeach
                                </select>
                                <br>
                                <br>
                                <label for="provincia_id" class="col-md-4 col-form-label text-md-right">Provincia</label>
                                <select
                                    name="provincia_id"
                                    id="provincia_id"
                                    class="provincia_id custom-select"
                                    required
                                    style="width:50%"

                                    >
                                    <option
                                        value="0"
                                        disabled="true"
                                        selected="true"
                                        title="Seleccione una provincia"
                                        >
                                        -Seleccione una provincia-
                                    </option>
                                </select>
                                <br>
                                <br>
                                <label for="ciudad_id" class="col-md-4 col-form-label text-md-right">Ciudad</label>
                                <select
                                    name="ciudad_id"
                                    id="ciudad_id"
                                    class="ciudad_id custom-select"
                                    required
                                    style="width:50%"

                                    >
                                    <option
                                        value="0"
                                        disabled="true"
                                        selected="true"
                                        title="Seleccione una ciudad"
                                        >
                                        -Seleccione una ciudad-
                                    </option>
                                </select>
                                <br>
                                <br>

                                <label for="barrio_id" class="col-md-4 col-form-label text-md-right">Barrio</label>

                                <select
                                    name="barrio_id"
                                    id="barrio_id"
                                    class="barrio_id custom-select"
                                    required
                                    style="width:50%"

                                    >
                                    <option
                                        value="0"
                                        disabled="true"
                                        selected="true"
                                        title="Seleccione un barrio"
                                        >
                                        -Seleccione un barrio-
                                    </option>
                                </select>
                                <br>

                            </div>
                            <br>


                        <div class="form-group row">
                            <label for="domicilio" class="col-md-4 col-form-label text-md-right">Domicilio</label>
                            <div class="col-md-6">
                                <input id="domicilio" type="string" class="form-control @error('domicilio') is-invalid @enderror" name="domicilio" value="{{ old('domicilio') }}" autocomplete="domicilio" autofocus>
                                @error('domicilio')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>





                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Crear
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('js')
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>

    <script>
            console.log('antes de entrar al script');
            $(document).ready(function() {
                console.log('jquery anda');


                $(document).on('change','.pais_id',function(){
                    console.log("se cambio el pais");
                    var pais_id=$(this).val();
                    console.log(pais_id);
                    var div=$(this).parent();
                    var op=" ";



                    $.ajax({
                        type:'get',
                        url:'{!!URL::to('proveedores/create/buscarProvincia')!!}',
                        data:{'id':pais_id},
                        success:function(data){
                            console.log('exito!');
                            console.log(data);
                            console.log(data.length);
                            op+='<option value="0" selected disabled>-Seleccione una provincia-</option>';
                            for(var i=0;i<data.length;i++){
                                op+='<option value="'+data[i].id+'">'+data[i].nombre+'</option>';
                            }
                            div.find('.provincia_id').html(" ");
                            div.find('.provincia_id').append(op);
                        },
                        error:function(){
                        }
                    });
                });


                $(document).on('change','.provincia_id',function(){
                    console.log("se cambio la provincia");
                    var provincia_id=$(this).val();
                    console.log(provincia_id);
                    var div=$(this).parent();
                    var op=" ";



                    $.ajax({
                        type:'get',
                        url:'{!!URL::to('proveedores/create/buscarCiudad')!!}',
                        data:{'id':provincia_id},
                        success:function(data){
                            console.log('success');
                            console.log(data);
                            console.log(data.length);
                            op+='<option value="0" selected disabled>-Seleccione una ciudad-</option>';
                            for(var i=0;i<data.length;i++){
                                op+='<option value="'+data[i].id+'">'+data[i].nombre+'</option>';
                            }
                            div.find('.ciudad_id').html(" ");
                            div.find('.ciudad_id').append(op);
                        },
                        error:function(){
                        }
                    });
                });

                $(document).on('change','.ciudad_id',function(){
                    console.log("se cambio la ciudad");
                    var ciudad_id=$(this).val();
                    console.log(ciudad_id);
                    var div=$(this).parent();
                    var op=" ";



                    $.ajax({
                        type:'get',
                        url:'{!!URL::to('proveedores/create/buscarBarrio')!!}',
                        data:{'id':ciudad_id},
                        success:function(data){
                            console.log('success');
                            console.log(data);
                            console.log(data.length);
                            op+='<option value="0" selected disabled>-Seleccione un barrio-</option>';
                            for(var i=0;i<data.length;i++){
                                op+='<option value="'+data[i].id+'">'+data[i].nombre+'</option>';
                            }
                            div.find('.barrio_id').html(" ");
                            div.find('.barrio_id').append(op);
                        },
                        error:function(){
                        }
                    });
                });



            });

    </script>
@endsection


