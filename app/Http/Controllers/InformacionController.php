<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InformacionController extends Controller
{
    public function miPerfil()
    {
    	$user=Auth::user();
        //dd($user);
        return view('informaciones.miPerfil', compact('user'));

	}
}
