@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header h3">
                    Proveedores
                  <a href="{{route('proveedores.create')}}">
                    <button title="agregar" class="btn btn-success btn-responsive">
                        <i class="fa fa-plus"></i> Nuevo
                    </button>
                </a>
                <a href="{{route('home')}}">
                    <button title="volver" class="btn btn-secondary btn-responsive">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Atrás
                    </button>
                </a>
                </div>
                <div class="card-body">
                    @include('layouts.mensaje')
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <table id="tabla" class="table table-bordered table-hover border-dark">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Razón Social</th>
                                    <th scope="col">Descripción</th>
                                    <th scope="col">E-mail</th>
                                    <th scope="col">Opciones</th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach($proveedores as $p)
                                        <tr>
                                            <td>{{ $p->razon_social }}</td>
                                            <td>{{ $p->descripcion }}</td>
                                            <td>{{ $p->email }}</td>
                                            <td colspan="3">
                                                <a href="{{route('proveedores.edit', $p)}}" type="button" class="btn btn-light btn-sm"><i class="fa fa-edit"></i> Editar</a>

                                                <a data-keyboard="false" data-target="#modal-delete-{{$p->id}}" data-toggle="modal">
                                                    <button title="eliminar" class="btn btn-danger btn-responsive">
                                                    <i class="fa fa-trash"></i> Eliminar
                                                    </button>
                                                </a>

                                                <a data-keyboard="false" data-target="#modal-ver-{{$p->id}}" data-toggle="modal">
                                                    <button title="eliminar" class="btn btn-primary btn-responsive">
                                                        <i class="fa fa-eye"></i> Ver
                                                    </button>
                                                </a>


                                            </td>
                                        </tr>
                                    @include('proveedores.ver')
                                    @include('proveedores.delete')
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#tabla').DataTable({
              "aaSorting": [],
              "pageLength" : 5,
              "lengthMenu": [[5, 10], [5, 10]],
              language: {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",

                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }

                }

            });




        } );
    </script>
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@endsection

