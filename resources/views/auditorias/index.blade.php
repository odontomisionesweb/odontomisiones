@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header h3">
                    Auditoria
                </div>
                <div class="card-body">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="card collapsed-card">

                            <div class="card-body">

                                @include('auditorias.buscar') <!-- elemento de la -->



                            </div>

                        </div>
                        <br><!-- salteamos un renglon -->
                        <table id="tabla" class="table table-bordered table-hover border-dark table-responsive">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Tabla</th>
                                    <th scope="col">ID</th>
                                    <th scope="col">Evento</th>
                                    <th scope="col">Usuario</th>
                                    <th scope="col">Valores anteriores</th>
                                    <th scope="col">Valores Nuevos</th>
                                    <th scope="col">Fecha</th>
                                    <th scope="col">Mas Detalles</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($audits as $a)
                                <tr>
                                    <td>{{$a->auditable_type }}</td>
                                    <td>{{$a->auditable_id }}</td> <!-- ID del registro modificado -->
                                    <td> <!-- Evento -->
                                        @if ($a->event == 'created')
                                            <span class="label label-success">Creación</span>
                                        @endif
                                        @if ($a->event == 'updated')
                                        <span class="label label-warning">Actualización</span>

                                        @endif
                                        @if ($a->event == 'deleted')
                                            <span class="label label-danger">Eliminación</span>
                                        @endif
                                    </td>
                                    <td>{{$a->user->nombre }}</td> <!-- Usuario Responsable -->
                                    <td>
                                        <table class="table table-bordered table-condensed table-hover table-striped" style="border:3px solid #357CA5 width:100%">
                                            @foreach($a->old_values as $attribute => $value) <!-- Valores Anteriores -->
                                            @if ($value!= NULL)
                                                <tr onmouseover="cambiar_color_over2(this)" onmouseout="cambiar_color_out(this)">
                                                    <td><b>{{ $attribute }}</b></td>
                                                    <td>{{ $value }}</td>
                                                </tr>
                                            @else
                                                <td>No hay datos previos</td>
                                            @endif

                                            @endforeach

                                        </table>
                                    </td>
                                    <td>
                                        <table class="table table-bordered table-condensed table-hover table-striped" style="border:3px solid #357CA5 width:100%">
                                            @forelse($a->new_values as $attribute => $value) <!-- Valores Nuevos -->
                                                <tr onmouseover="cambiar_color_over2(this)" onmouseout="cambiar_color_out(this)">
                                                    <td><b>{{ $attribute }}</b></td>
                                                    <td>{{ $value }}</td>
                                                </tr>
                                                @empty
                                                    No hay valores nuevos
                                            @endforelse

                                        </table>
                                    </td>
                                    <td>{{$a->created_at->format('d/m/Y h:i:s A') }}</td> <!-- Fecha -->

                                    <td style="text-align: center" colspan="1"> <!-- Opciones -->
                                        <a href="" data-target="#modal-show-{{$a->id}}" data-toggle="modal">
                                                <button title="ver" class="btn btn-warning btn-responsive">
                                                <i class="fa fa-eye"></i>
                                            </button>
                                        </a>
                                    </td>

                                </tr>
                                @include('auditorias.modalshow')


                            @endforeach


                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#tabla').DataTable({
              "aaSorting": [],
              "pageLength" : 5,
              "lengthMenu": [[5, 10], [5, 10]],
              language: {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",

                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }

                }

            });




        } );
    </script>
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@endsection

