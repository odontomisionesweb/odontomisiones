

    {!! Form::model(Request::only(
        ['desde','hasta','auditable_type']),
        ['url' => 'auditorias', 'method'=>'GET', 'autocomplete'=>'on', 'role'=>'search']
        )!!}

        <div class="form-group row">
            <label for="auditable_type" class="col-md-4 col-form-label text-md-right">Tabla</label>
            <div class="col-md-6">
                <select
                    name="auditable_type"
                    id="auditable_type"
                    class="auditable_type custom-select"
                    >
                    @foreach ($types as $t)
                        <option
                            value="{{$t}}"
                            @if($auditable_type!=null && $auditable_type==$t)
                                selected
                            @endif
                        >
                        {{$t}}
                        </option>
                    @endforeach

                    <option
                        value="0"
                        @if($auditable_type == null || $auditable_type==0)
                            selected
                        @endif
                    >
                        -- Todas las Tablas --
                    </option>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="desde" class="col-md-4 col-form-label text-md-right">ID de la Tabla</label>
            <div class="col-md-6">
                <input
                    type="numeric"
                    name="auditable_id"
                    id="auditable_id"
                    class="auditable_id form-control"
                    value="{{$auditable_id}}"
                >
            </div>
        </div>

        <div class="form-group row">
            <label for="desde" class="col-md-4 col-form-label text-md-right">Fecha Desde</label>
            <div class="col-md-6">
                <input
                    type="date"
                    name="desde"
                    id="desde"
                    class="fecha form-control"
                    value="{{$desde}}"
                >
            </div>
        </div>

        <div class="form-group row">
            <label for="hasta" class="col-md-4 col-form-label text-md-right">Fecha Hasta</label>
            <div class="col-md-6">
                <input
                    type="date"
                    name="hasta"
                    id="hasta"
                    class="fecha form-control"
                    value="{{$hasta}}"
                    >
            </div>
        </div>


        <div class="form-group row">
            <label for="buscar" class="col-md-4 col-form-label text-md-right"></label>
            <div class="col-md-6">
                <span class="input-group-btn">
                    <button
                        title="buscar"
                        type="submit"
                        id="bt_add"
                        name="filtrar"
                        class="btn btn-primary btn-responsive">
                            <i class="fa fa-filter"></i> Filtrar
                    </button>

                    <a

                    href= "{{ route('auditorias.index') }}"
                    class="btn btn-light"
                    >
                    <i class="fas fa-eraser"></i>
                        ... Limpiar
                </a>

                </span>
            </div>
        </div>






    {{Form::close()}}
<script type="text/javascript">
$(document).ready(function(){


    //si existe un cambio en Fecha Desde
    $('#desde').change(function() {

        //Se captura su valor
        var desde = $(this).val();
        console.log(desde, 'Se cambio la fecha DESDE')
        //y establesco ese valor capturado como minimo en Fecha Hasta
        $('#hasta').attr({"min" : desde});;


        });

    //si existe un cambio en Fecha Hasta
    $('#hasta').change(function() {

        //Se captura su valor
        var hasta = $(this).val();
        console.log(hasta, 'Se cambio la fecha HASTA');

        //si ese valor es diferente a nulo (osea si hay algo dentro)
        if (hasta != "")
        {
            //se deshabilita el desde (para evitar que desde sea mayor que hasta)
            $("#desde").prop('disabled', true);

        }



      });

     //si se clickea en "FIltrar"
      $('#bt_add').click(function () {
        //se debe refrescar (si es que hubo) la prop disabled de desde
        $("#desde").prop('disabled', false);


      });


});







</script>

