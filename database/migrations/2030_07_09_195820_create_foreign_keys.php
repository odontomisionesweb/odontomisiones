<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pedidos', function (Blueprint $table) {
            $table->foreign('proveedor_id')->references('id')->on('proveedores');
            $table->foreign('user_id')->references('id')->on('users');
        });
        Schema::table('provincias', function (Blueprint $table) {
            $table->foreign('pais_id')->references('id')->on('paises');
        });
        Schema::table('ciudades', function (Blueprint $table) {
            $table->foreign('provincia_id')->references('id')->on('provincias');
        });
        Schema::table('barrios', function (Blueprint $table) {
            //singular--plural
            $table->foreign('ciudad_id')->references('id')->on('ciudades');
        });
        Schema::table('equipos', function (Blueprint $table) {
            $table->foreign('estados_equipos_id')->references('id')->on('estados_equipos');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('barrio_id')->references('id')->on('barrios');
            $table->foreign('rol_id')->references('id')->on('roles');
            //$table->foreign('estado_crediticio_id')->references('id')->on('estado_crediticios');

        });

        Schema::table('detalle_pedidos', function (Blueprint $table) {
            $table->foreign('pedido_id')->references('id')->on('pedidos');
            $table->foreign('elemento_id')->references('id')->on('elementos');
        });

        Schema::table('incidencias', function (Blueprint $table) {
            $table->foreign('prioridad_id')->references('id')->on('prioridades');
            $table->foreign('tecnico_id')->references('id')->on('users');
            $table->foreign('cliente_id')->references('id')->on('users');
            $table->foreign('equipo_id')->references('id')->on('equipos');
            $table->foreign('estado_actual_id')->references('id')->on('estados_incidencias');

        });

        Schema::table('historial_estados', function (Blueprint $table) {
            $table->foreign('incidencia_id')->references('id')->on('incidencias');
            $table->foreign('estado_id')->references('id')->on('estados_incidencias');
        });

        Schema::table('recepciones', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('pedido_id')->references('id')->on('pedidos');
        });

        Schema::table('detalle_recepciones', function (Blueprint $table) {
            $table->foreign('elemento_id')->references('id')->on('elementos');
            $table->foreign('recepcion_id')->references('id')->on('recepciones');
        });

        Schema::table('consumos', function (Blueprint $table) {
            $table->foreign('incidencia_id')->references('id')->on('incidencias');
            $table->foreign('elemento_id')->references('id')->on('elementos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pedidos', function (Blueprint $table) {
            $table->dropForeign(['proveedor_id']);
        });
        Schema::table('provincias', function (Blueprint $table) {
            $table->dropForeign('pais_id');
        });
        Schema::table('ciudades', function (Blueprint $table) {
            $table->dropForeign('provincia_id');
        });
        Schema::table('barrios', function (Blueprint $table) {
            $table->dropForeign('ciudad_id');
        });
        Schema::table('equipos', function (Blueprint $table) {
            $table->dropForeign('estado_equipo_id');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('barrio_id');
            //$table->dropForeign('estado_crediticio_id');
            $table->dropForeign('rol_id');

        });

        Schema::table('historial_estados', function (Blueprint $table) {
            $table->dropForeign('incidencia_id');
            $table->dropForeign('estado_id');
        });

        Schema::table('recepciones', function (Blueprint $table) {
            $table->dropForeign('user_id');
            $table->dropForeign('pedido_id');
        });
        Schema::table('detalle_recepciones', function (Blueprint $table) {
            $table->dropForeign('elemento_id');
            $table->dropForeign('recepcion_id');
        });
        Schema::table('consumos', function (Blueprint $table) {
            $table->dropForeign('incidencia_id');
            $table->dropForeign('elemento_id');
        });
    }
}
