<?php

use Illuminate\Database\Seeder;

class ProvinciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('provincias')->insert([
            'codigo'=> 'MI',
            'pais_id'=> 1,
            'nombre'=> 'Misiones',
            ]);
    
        DB::table('provincias')->insert([
        'codigo'=> 'AS',
        'pais_id'=> 2,
        'nombre'=> 'Asuncion',
        ]);

        DB::table('provincias')->insert([
            'codigo'=> 'MV',
        'pais_id'=> 3,
        'nombre'=> 'Montevideo',
        ]);
    }
}
