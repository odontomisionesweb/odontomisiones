<div class="modal fade modal-slide-in-right"
     aria-hidden="true"
     role="dialog"
     tabindex="-1"
     id="modal-ver-{{Auth::user()->id}}">



        <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header" style="background-color:#3490DC">
              <h3 class="modal-title" style="color: white"><i style="color: white" class="fas fa-id-card" aria-hidden="true"></i> Mi Perfil</h3>
              </div>
              <div class="modal-body" style="color: black">
                <ul>
                  <li style="font-size:100%"><b>Apellidos y Nombres :</b> {{auth()->user()->apellido}} {{auth()->user()->nombre}}</li>

                  <li style="font-size:100%"><b>Teléfono: </b>{{auth()->user()->telefono}} </li>
                  <li style="font-size:100%"><b>Correo Electrónico: </b>{{auth()->user()->email}} </li>
                  <li style="font-size:100%"><b>Dirección: </b>{{auth()->user()->direccion()}} </li>
                  <li style="font-size:100%"><b>Rol: </b>{{auth()->user()->rol->nombre}} </li>
                </ul>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>


              </div>
            </div>
        </div>





</div>

