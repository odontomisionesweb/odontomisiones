<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Requests\PaisRequest;
use App\Pais;

class PaisController extends Controller
{
    public function index()
    {
        $paises=Pais::orderBy('nombre')->get();
        return view('paises.index',compact('paises'));
    }

    public function create()
    {
        return view('paises.create');
    }

    public function store(Request $request, Pais $pais)
    {
        $request->validate([
            'codigo'=>[ Rule::unique('paises'),
                        'required', 'regex:/^[a-zA-Z]+$/u', 'max:5' ],

            //'codigo' => 'unique:paises|required|max:5|regex:/^[a-zA-Z]+$/u',//regex sin espacios
            'nombre'=>'required|regex:/^[a-zA-Z][a-zA-Z ]+$/u|max:50',//regex con espacios
        ], [
            'required' => 'Campo requerido',
            'codigo.unique' => 'Está ingresando un código existente',
            'regex' => 'Complete el campo únicamente con letras',
            //no toma
            'codigo.max' => 'Se permiten hasta 5 caracteres',
            'nombre.max' => 'Se permiten hasta 50 caracteres',
        ]);

        $pais=Pais::create([
            'codigo'=>$request->codigo,
            'nombre'=>$request->nombre,
        ]);

        return redirect()->route('paises.index')->withMessage('País creado');
    }

    public function edit(Pais $pais)
    {
        return view('paises.edit',compact('pais'));
    }

    public function update(Request $request, Pais $pais)
    {
        $request->validate([
            'codigo'=>[ Rule::unique('paises')->ignore($pais),
                        'required', 'regex:/^[a-zA-Z]+$/u', 'max:5' ],

            //'codigo' => 'unique:paises|required|max:5|regex:/^[a-zA-Z]+$/u',//regex sin espacios
            'nombre'=>'required|regex:/^[a-zA-Z][a-zA-Z ]+$/u|max:50',//regex con espacios
        ], [
            'required' => 'Campo requerido',
            'codigo.unique' => 'Está ingresando un código existente',
            'regex' => 'Complete el campo unicamente con letras',
            //no toma
            'codigo.max' => 'Se permiten hasta 5 caracteres',
            'nombre.max' => 'Se permiten hasta 50 caracteres',
        ]);
        $pais->update([
            'codigo' => $request->codigo,
            'nombre' =>$request->nombre,
        ]);

        return redirect()->route('paises.index')->withMessage('País modificado');
    }

    public function destroy(Pais $pais) {

        //dd("destroy");

        if ($pais->existe_referencia()) {
            return redirect()->route('paises.index')->withErrors('No se puede eliminar el país, hay referencias asociadas');
        }

        $pais->delete();
        return redirect()->route('paises.index')->with('message','País eliminado');




    }
}
