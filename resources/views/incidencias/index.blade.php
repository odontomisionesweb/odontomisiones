@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">

                <div class="card-header h3">
                    Incidencias
                  <a href="{{route('incidencias.create')}}">
                    <button title="agregar" class="btn btn-success btn-responsive">
                        <i class="fa fa-plus"></i> Nuevo
                    </button>
                </a>
                </div>
                <div class="card-body">
                    <table id="tabla" class="table table-bordered table-hover border-dark">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Descripcion</th>
                                <th scope="col">Nota</th>
                                <th scope="col">Fecha de Creación</th>
                                <th scope="col">Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($incidencias as $incidencia)
                                <tr>
                                    <th>{{ $incidencia->id }}</th>
                                    <td>{{ $incidencia->descripcion }}</td>
                                    <td>{{ $incidencia->nota }}</td>
                                    <td>{{ $incidencia->created_at }}</td>
                                    <td>
                                <form method="post" action="" >
                                    <a href="" type="button" class="btn btn-light btn-sm"><i class="fa fa-edit"></i> Editar</a>
                                    <a href="" type="button" class="btn btn-info btn-sm"><i class="fa fa-eye"></i> Ver</a>
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button type="submit" onclick="return confirm('¿Deseas borrar?')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Borrar</button>

                                </form>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
              </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#tabla').DataTable({
              "aaSorting": [],
              "pageLength" : 5,
              "lengthMenu": [[5, 10], [5, 10]],
              language: {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",

                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }

                }

            });




        } );
    </script>
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@endsection

