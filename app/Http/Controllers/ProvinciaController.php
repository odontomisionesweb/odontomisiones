<?php

namespace App\Http\Controllers;
use App\Provincia;
use App\Pais;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class provinciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $paises=Pais::all();

        if(count($request->all())>1)
        {
            //dd($request->all());
            $sql = Provincia::select('provincias.*');

            if($request->pais_id)
            {
                $sql = $sql->wherePais_id($request->pais_id);
            }


            $provincias=$sql->orderBy('created_at','desc')->get();
            $pais_id=$request->pais_id;


        }
        else
        {
            $pais_id=null;

            $provincias=Provincia::orderBy('created_at','desc')->get();


        }

        return view('provincias.index',[
            "provincias"         =>  $provincias,
            "pais_id"        =>  $pais_id,
            "paises"         =>  $paises,

            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $paises=Pais::all();
        return view('provincias.create', compact('paises'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Provincia $provincia)
    {
        $request->validate([
            'codigo'=>[ Rule::unique('provincias'),
                        'required', 'regex:/^[a-zA-Z]+$/u', 'max:5' ],

            //'codigo' => 'unique:paises|required|max:5|regex:/^[a-zA-Z]+$/u',//regex sin espacios
            'nombre'=>'required|regex:/^[a-zA-Z][a-zA-Z ]+$/u|max:50',//regex con espacios
        ], [
            'required' => 'Campo requerido',
            'codigo.unique' => 'Está ingresando un código existente',
            'regex' => 'Complete el campo únicamente con letras',
            //no toma
            'codigo.max' => 'Se permiten hasta 5 caracteres',
            'nombre.max' => 'Se permiten hasta 50 caracteres',
        ]);
        $provincia=Provincia::create([
            'codigo'=>$request->codigo,
            'nombre'=>$request->nombre,
            'pais_id'=>$request->pais_id,
        ]);

        //$provincia->pais->nombre;

        return redirect()->route('provincias.index')->withMessage('Provincia creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Provincia $provincia)
    {
        $paises=Pais::all();
        return view('provincias.edit',compact('provincia', 'paises'));
    }

    public function update(Request $request, Provincia $provincia)
    {
        $request->validate([
            'codigo'=>[ Rule::unique('provincias')->ignore($provincia),
                        'required', 'regex:/^[a-zA-Z]+$/u', 'max:5' ],

            //'codigo' => 'unique:paises|required|max:5|regex:/^[a-zA-Z]+$/u',//regex sin espacios
            'nombre'=>'required|regex:/^[a-zA-Z][a-zA-Z ]+$/u|max:50',//regex con espacios
        ], [
            'required' => 'Campo requerido',
            'codigo.unique' => 'Está ingresando un código existente',
            'regex' => 'Complete el campo unicamente con letras',
            //no toma
            'codigo.max' => 'Se permiten hasta 5 caracteres',
            'nombre.max' => 'Se permiten hasta 50 caracteres',
        ]);

        $provincia->update([
            'codigo'=>$request->codigo,
            'nombre'=>$request->nombre,
            'pais_id' =>$request->pais_id,

        ]);

        return redirect()->route('provincias.index')->withMessage('Provincia modificada');
    }

    public function destroy(Provincia $provincia) {

        //dd("destroy");

        if ($provincia->existe_referencia()) {
            return redirect()->route('provincias.index')->withErrors('No se puede eliminar la provincia, hay referencias asociadas');
        }

        $provincia->delete();
        return redirect()->route('provincias.index')->with('message','Provincia eliminada');




    }
}
