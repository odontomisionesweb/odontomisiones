<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Validator;
class CiudadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'provincia_id' => 'required',//regex sin espacios
            'nombre'=>'required|regex:/^[a-zA-Z][a-zA-Z ]+$/u|max:255',//regex con espacios
            
        ];
    }

    public function messages() {
        return [
            'required' => 'Campo requerido',
            'codigo.unique' => 'Está ingresando un código existente',
            'regex' => 'Complete el campo unicamente con letras',

        ];

    }

}
