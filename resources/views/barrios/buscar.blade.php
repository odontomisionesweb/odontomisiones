

    {!! Form::model(Request::only(
        ['ciudad_id']),
        ['url' => 'barrios/', 'method'=>'GET', 'autocomplete'=>'on', 'role'=>'search']

        )!!}

        <div class="form-group row">
            <label for="ciudad_id" class="col-md-4 col-form-label text-md-right">Ciudad</label>
            <div class="col-md-6">
                <select
                    name="ciudad_id"
                    id="ciudad_id"
                    class="ciudad_id custom-select"
                    >
                    @foreach ($ciudades as $ciudad)
                        <option
                            value="{{$ciudad->id}}"
                            @if($ciudad_id!=null && $ciudad_id==$ciudad->id)
                                selected
                            @endif
                        >
                        {{$ciudad->nombre}}
                        </option>
                    @endforeach
                    <option
                        value="0"
                        @if($ciudad_id == null || $ciudad_id==0)
                            selected
                        @endif
                    >
                        -- Todas las ciudades --
                    </option>
                </select>
            </div>

        </div>



        <div class="form-group row">
            <label for="buscar" class="col-md-4 col-form-label text-md-right"></label>
            <div class="col-md-6">
                <span class="input-group-btn">
                    <button
                        title="buscar"
                        type="submit"
                        id="bt_add"
                        name="filtrar"
                        class="btn btn-primary btn-responsive">
                            <i class="fa fa-filter"></i> Filtrar
                    </button>

                    <a

                    href= "{{ route('barrios.index') }}"
                    class="btn btn-light"
                    >
                    <i class="fas fa-eraser"></i>
                        ... Limpiar
                </a>

                </span>
            </div>
        </div>






    {{Form::close()}}
<script type="text/javascript">
$(document).ready(function(){


    //si existe un cambio en Fecha Desde
    $('#desde').change(function() {

        //Se captura su valor
        var desde = $(this).val();
        console.log(desde, 'Se cambio la fecha DESDE')
        //y establesco ese valor capturado como minimo en Fecha Hasta
        $('#hasta').attr({"min" : desde});;


        });

    //si existe un cambio en Fecha Hasta
    $('#hasta').change(function() {

        //Se captura su valor
        var hasta = $(this).val();
        console.log(hasta, 'Se cambio la fecha HASTA');

        //si ese valor es diferente a nulo (osea si hay algo dentro)
        if (hasta != "")
        {
            //se deshabilita el desde (para evitar que desde sea mayor que hasta)
            $("#desde").prop('disabled', true);

        }



      });

     //si se clickea en "FIltrar"
      $('#bt_add').click(function () {
        //se debe refrescar (si es que hubo) la prop disabled de desde
        $("#desde").prop('disabled', false);


      });


});







</script>

