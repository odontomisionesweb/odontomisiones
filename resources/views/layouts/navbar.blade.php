<div class="container">
    <a class="navbar-brand text-white" href="{{ url('/home') }}">
        OdontoMisiones
    </a>
    @guest
    @else
    @if(auth()->user()->rol_id != 4)
        <a class="nav-link text-white" href="{{ route('home') }}">
            <i class="fas fa-home"></i>&nbsp; Inicio
        </a>
    @endif
    @endguest

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <!-- Left Side Of Navbar -->
        <ul class="navbar-nav mr-auto">

        </ul>

        <!-- Right Side Of Navbar -->
        <ul class="navbar-nav ml-auto">
            <!-- Authentication Links -->

            @guest
                <li class="nav-item">
                    <a class="nav-link text-white" href="{{ route('login') }}">Iniciar Sesión</a>
                </li>

                <!--li class="nav-item">
                    <a class="nav-link text-white" href="{{ route('register') }}">Registrarse</a>
                </li-->

            @else

                @if(auth()->user()->rol_id == 1)

                <li class="nav-item">
                    <a class="nav-link text-white" href="{{ route('proveedores.index') }}">
                        <i class="fas fa-user-friends"></i> Proveedores
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link text-white" href="{{ route('users.index') }}">
                        <i class="fa fa-users"></i> Usuarios
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link text-white" href="{{ route('repuestos.index') }}">
                        <i class="fas fa-hammer"></i> Repuestos
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link text-white" href="#">
                        <i class="fas fa-box"></i> Pedidos
                    </a>
                </li>

                <div class="dropdown show">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle text-white" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        <i class="fa fa-cogs"></i> Parámetros
                    </a>


                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="{{ route('paises.index') }}"> <i class="fas fa-globe"></i> Países</a>
                        <a class="dropdown-item" href="{{ route('provincias.index') }}"> <i class="fas fa-globe-americas"></i> Provincias</a>
                        <a class="dropdown-item" href="{{ route('ciudades.index') }}"> <i class="fas fa-city"></i> Ciudades</a>
                        <a class="dropdown-item" href="{{ route('barrios.index') }}"> <i class="far fa-building"></i> Barrios</a>
                    </div>
                </div>
                @endif


                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle text-white" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        <i class="fas fa-user"></i> Perfil <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                        <a class="dropdown-item" data-keyboard="false" data-target="#modal-ver-{{Auth::user()->id}}" data-toggle="modal">
                            <i class="fas fa-user"></i>&nbsp; Mi Perfil
                        </a>


                        <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            <i class="fas fa-sign-out-alt"></i>&nbsp; Salir
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>

            @endguest
        </ul>
    </div>
    @guest
    @else
        @include('informaciones.miPerfil')
    @endguest

</div>
