<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipo extends Modelz
{
    protected $fillable = [
        'modelo',
        'fecha_garantia',
        'num_serie',
        'estado_equipos_id',
    ];

}
