<?php

use Illuminate\Database\Seeder;

class PedidoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pedidos')->insert([
        'fecha_entrada'=> '2020-05-01',
        'proveedor_id'=>1,
        'user_id'=>1,
        'descripcion'=>'aaaaa',
        ]);

        DB::table('pedidos')->insert([
        'fecha_entrada'=> '2020-06-01',
        'proveedor_id'=>2,
        'user_id'=>2,
        'descripcion'=>'aaaaa',
        ]);

        DB::table('pedidos')->insert([
        'fecha_entrada'=> '2020-04-15',
        'proveedor_id'=>3,
        'user_id'=>3,
        'descripcion'=>'aaaaa',
        ]);
    }

}
