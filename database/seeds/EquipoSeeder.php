<?php

use Illuminate\Database\Seeder;

class EquipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('equipos')->insert([
        'marca'=> 'Samsung',
        'modelo'=>'2019',
        'fecha_garantia'=>'2020-09-01',
        'num_serie'=>111,
        'estados_equipos_id'=>1,
        ]);

        DB::table('equipos')->insert([
        'marca'=> 'Hitachi',
        'modelo'=>'2020',
        'fecha_garantia'=>'2020-12-01',
        'num_serie'=>112,
        'estados_equipos_id'=>1,
        ]);

        DB::table('equipos')->insert([
        'marca'=> 'OtraMarca',
        'modelo'=>'2018',
        'fecha_garantia'=>'2020-11-01',
        'num_serie'=>132,
        'estados_equipos_id'=>1,
        ]);
    }
}


