<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    public $timestamps = false;

    protected $fillable = ['fecha_entrada','descripcion', 'proveedor_id', 'user_id'];

    protected $table = 'pedidos';

    public function proveedor(){
       return $this->belongsTo('App\Proveedor');
    }

    public function user(){
       return $this->belongsTo('App\User');
    }


}
