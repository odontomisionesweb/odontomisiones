@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">

                <div class="card-header h3">
                    Editar Ciudad
                    <a href="{{route('ciudades.index')}}">
                    <button title="volver" class="btn btn-secondary btn-responsive">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Atrás
                    </button>
                </a>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('ciudades.update', $ciudad) }}">
                        {{ csrf_field() }}
                        {{ method_field('put') }}

                        <div class="form-group row">
                            <label for="codigo" class="col-md-4 col-form-label text-md-right">Código</label>

                            <div class="col-md-2">
                                <input id="codigo" type="codigo" class="form-control @error('codigo') is-invalid @enderror" name="codigo" value="{{old('codigo', $ciudad->codigo)}}" autocomplete="codigo" autofocus>

                                @error('codigo')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="nombre" class="col-md-4 col-form-label text-md-right">Nombre</label>

                            <div class="col-md-6">
                                <input id="nombre" type="string" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{$ciudad->nombre}}" required autocomplete="nombre" autofocus>

                                @error('nombre')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>    
                        <div class="form-group row">
                            <label for="provincia_id" class="col-md-4 col-form-label text-md-right">Provincia</label>
                            <div class="col-md-6">
                               
                                
                                <select class="custom-select" id="provincia_id" name="provincia_id" required>
                                
                                @foreach($provincias as $p)
                                    @if($p->id == $ciudad->provincia_id)
                                        <option value="{{$p->id}}" selected>{{$p->nombre}}</option>
                                    @else
                                        <option value="{{$p->id}}">{{$p->nombre}}</option>
                                    @endif
                                @endforeach 

                                </select>
                                <div class="invalid-tooltip">
                                  Por favor seleccione una provincia valida
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="cod_postal" class="col-md-4 col-form-label text-md-right">Código Postal</label>

                            <div class="col-md-2">
                                <input id="cod_postal" type="cod_postal" class="form-control @error('cod_postal') is-invalid @enderror" name="cod_postal" value="{{old('cod_postal', $ciudad->cod_postal)}}" autocomplete="cod_postal" autofocus>

                                @error('cod_postal')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Editar
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection