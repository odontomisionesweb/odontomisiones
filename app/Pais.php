<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Pais extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    public $timestamps = false;

    protected $fillable = ['codigo','nombre'];

    protected $table = 'paises';

    public function existe_referencia()
    {

        $ref = Provincia::wherePais_id($this->id)->count();
        //dd($ref);

        if($ref > 0) //existe referencia
        {
            return true;
        }
        else    //no hay referencia
        {
            return false;
        }

    }

}
