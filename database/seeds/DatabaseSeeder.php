<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolSeeder::class);

        $this->call(PaisSeeder::class);
    	$this->call(ProvinciaSeeder::class);
    	$this->call(CiudadSeeder::class);
        $this->call(BarrioSeeder::class);
        $this->call(EstadoCrediticioSeeder::class);
    	$this->call(UserSeeder::class);
    	//$this->call(EstadoEquipoSeeder::class);
        //$this->call(EquipoSeeder::class);


        //$this->call(EstadoIncidenciaSeeder::class);
    	$this->call(ElementoSeeder::class);
    	$this->call(ProveedorSeeder::class);
    	//$this->call(PrioridadSeeder::class);
    	//$this->call(PedidoSeeder::class);
    	//$this->call(IncidenciaSeeder::class);

        //$this->call(HistorialEstadoSeeder::class);
    }
}
