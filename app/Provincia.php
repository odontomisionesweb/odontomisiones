<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;


class Provincia extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    public $timestamps = false;

    protected $fillable = ['codigo','nombre', 'pais_id'];

    protected $table = 'provincias';

    public function pais(){
        return $this->belongsTo('App\Pais');
    }

    public function existe_referencia()
    {

        $ref = Ciudad::whereProvincia_id($this->id)->count();
        //dd($ref);

        if($ref > 0) //existe referencia
        {
            return true;
        }
        else    //no hay referencia
        {
            return false;
        }

    }
}
