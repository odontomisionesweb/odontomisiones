<?php

use Illuminate\Database\Seeder;

class PaisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('paises')->insert([
            'codigo'=> 'AR',
            'nombre'=> 'Argentina',
            ]);
    
            DB::table('paises')->insert([
            'codigo'=> 'PY',
            'nombre'=> 'Paraguay',
            ]);
    
            DB::table('paises')->insert([
            'codigo'=> 'UY',
            'nombre'=> 'Uruguay',
        ]);
    }
}
