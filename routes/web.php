<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//Si el usuario está autenticado
Route::middleware(['auth'])->group(function() {




    //------------------------------Rutas de Incidencia------------------------------
    Route::get('incidencia', 'IncidenciaController@index')->name('incidencias.index');

    Route::get('incidencia/create', 'IncidenciaController@create')->name('incidencias.create');



    //------------------------------Rutas de Users------------------------------
    //Permisos de administrador
    Route::middleware(['administrador'])->group(function() {
                //------------------------------Rutas de Provincia------------------------------
            Route::get('provincias', 'ProvinciaController@index')->name('provincias.index');

            Route::get('provincias/create', 'ProvinciaController@create')->name('provincias.create');

            Route::post('provincias/create', 'ProvinciaController@store')->name('provincias.store');

            Route::get('provincias/edit/{provincia}',  'ProvinciaController@edit')->name('provincias.edit');
                    Route::put('provincia/edit/{provincia}',  'ProvinciaController@update')->name('provincias.update');

            Route::delete('provincias/destroy/{provincia}', 'ProvinciaController@destroy')->name('provincias.destroy');


            //------------------------------Rutas de ciudad------------------------------
            Route::prefix('ciudades')->group(function() {
                Route::get('/', 'CiudadController@index')->name('ciudades.index');

                Route::get('create', 'CiudadController@create')->name('ciudades.create');

                Route::post('create', 'CiudadController@store')->name('ciudades.store');


                Route::get('edit/{ciudad}',  'CiudadController@edit')->name('ciudades.edit');
                Route::put('edit/{ciudad}',  'CiudadController@update')->name('ciudades.update');

                Route::delete('destroy/{ciudad}', 'CiudadController@destroy')->name('ciudades.destroy');
            });
            //------------------------------Rutas de Paises------------------------------

            Route::prefix('paises')->group(function() {

                Route::get('/', 'PaisController@index')->name('paises.index');

                    Route::get('create', 'PaisController@create')->name('paises.create');
                    Route::post('create', 'PaisController@store')->name('paises.store');

                    Route::get('edit/{pais}',  'PaisController@edit')->name('paises.edit');
                    Route::put('edit/{pais}',  'PaisController@update')->name('paises.update');

                    Route::delete('destroy/{pais}', 'PaisController@destroy')->name('paises.destroy');
                });

            //------------------------------Rutas de Barrios------------------------------

            Route::get('barrios', 'BarrioController@index')->name('barrios.index');

            Route::get('barrios/create', 'BarrioController@create')->name('barrios.create');

            Route::post('barrios/create', 'BarrioController@store')->name('barrios.store');

            Route::get('barrios/edit/{barrio}',  'BarrioController@edit')->name('barrios.edit');
            Route::put('barrios/edit/{barrio}',  'BarrioController@update')->name('barrios.update');

            Route::delete('barrios/destroy/{barrio}', 'BarrioController@destroy')->name('barrios.destroy');

            //------------------------------Rutas de proveedores------------------------------
            Route::prefix('proveedores')->group(function() {
                Route::get('/', 'ProveedorController@index')->name('proveedores.index');

                Route::get('create', 'ProveedorController@create')->name('proveedores.create');
                    Route::post('create', 'ProveedorController@store')->name('proveedores.store');

                Route::get('edit/{proveedor}',  'ProveedorController@edit')->name('proveedores.edit');
                    Route::put('edit/{proveedor}',  'ProveedorController@update')->name('proveedores.update');


                Route::delete('destroy/{proveedor}', 'ProveedorController@destroy')->name('proveedores.destroy');

                Route::get('create/buscarProvincia', 'ProveedorController@buscarProvincia');
                Route::get('create/buscarCiudad', 'ProveedorController@buscarCiudad');
                Route::get('create/buscarBarrio', 'ProveedorController@buscarBarrio');

                Route::get('edit/{proveedor}/buscarProvincia', 'ProveedorController@buscarProvincia');
                Route::get('edit/{proveedor}/buscarCiudad', 'ProveedorController@buscarCiudad');
                Route::get('edit/{proveedor}/buscarBarrio', 'ProveedorController@buscarBarrio');
            });



                Route::prefix('users')->group(function() {
                    Route::get('/', 'UserController@index')->name('users.index');

                    Route::get('create', 'UserController@create')->name('users.create');
                    Route::post('create', 'UserController@store')->name('users.store');

                    Route::get('edit/{user}',  'UserController@edit')->name('users.edit');
                    Route::put('edit/{user}',  'UserController@update')->name('users.update');

                    Route::delete('destroy/{user}', 'UserController@destroy')->name('users.destroy');

                    Route::get('create/buscarProvincia', 'UserController@buscarProvincia');
                    Route::get('create/buscarCiudad', 'UserController@buscarCiudad');
                    Route::get('create/buscarBarrio', 'UserController@buscarBarrio');
                });


                Route::prefix('repuestos')->group(function() {
                    Route::get('/', 'RepuestoController@index')->name('repuestos.index');

                    Route::get('create', 'RepuestoController@create')->name('repuestos.create');
                    Route::post('create', 'RepuestoController@store')->name('repuestos.store');

                    Route::get('edit/{repuesto}',  'RepuestoController@edit')->name('repuestos.edit');
                    Route::put('edit/{repuesto}',  'RepuestoController@update')->name('repuestos.update');

                    Route::delete('destroy/{repuesto}', 'RepuestoController@destroy')->name('repuestos.destroy');

                    Route::get('create/buscarProvincia', 'RepuestoController@buscarProvincia');
                    Route::get('create/buscarCiudad', 'RepuestoController@buscarCiudad');
                    Route::get('create/buscarBarrio', 'RepuestoController@buscarBarrio');
                });

            });



            Route::prefix('informaciones')->group(function() {
                Route::get('/', 'InformacionController@index')->name('informaciones');
                Route::get('miPerfil', 'InformacionController@miPerfil')->name('informaciones.miPerfil');
            });


    Route::middleware(['auditor'])->group(function() {
        Route::prefix('auditorias')->group(function() {
            Route::get('/', 'AuditoriaController@index')->name('auditorias.index');

        });
    });



});
