{!! Form::model(Request::only(
        ['pais_id']),
        ['url' => 'provincias/', 'method'=>'GET', 'autocomplete'=>'on', 'role'=>'search']

        )!!}

        <div class="form-group row">
            <label for="pais_id" class="col-md-4 col-form-label text-md-right">País</label>
            <div class="col-md-6">
                <select
                    name="pais_id"
                    id="pais_id"
                    class="pais_id custom-select"
                    >
                    @foreach ($paises as $pais)
                        <option
                            value="{{$pais->id}}"
                            @if($pais_id!=null && $pais_id==$pais->id)
                                selected
                            @endif
                        >
                        {{$pais->nombre}}
                        </option>
                    @endforeach
                    <option
                        value="0"
                        @if($pais_id == null || $pais_id==0)
                            selected
                        @endif
                    >
                        -- Todas los paises --
                    </option>
                </select>
            </div>

        </div>


        <div class="form-group row">
            <label for="buscar" class="col-md-4 col-form-label text-md-right"></label>
            <div class="col-md-6">
                <span class="input-group-btn">
                    <button
                        title="buscar"
                        type="submit"
                        id="bt_add"
                        name="filtrar"
                        class="btn btn-primary btn-responsive">
                            <i class="fa fa-filter"></i> Filtrar
                    </button>

                    <a

                    href= "{{ route('provincias.index') }}"
                    class="btn btn-light"
                    >
                    <i class="fas fa-eraser"></i>
                        ... Limpiar
                </a>

                </span>
            </div>
        </div>






    {{Form::close()}}


