<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncidenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incidencias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion');
            $table->string('nota')->nullable();
            $table->unsignedBigInteger('prioridad_id');
            $table->unsignedBigInteger('estado_actual_id');
            $table->unsignedBigInteger('tecnico_id');
            $table->unsignedBigInteger('cliente_id');
            //una incidencia por equipo
            $table->unsignedBigInteger('equipo_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incidencias');
    }
}
