<?php

use Illuminate\Database\Seeder;

class HistorialEstadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('historial_estados')->insert([
        'incidencia_id'=> 1,
        'estado_id'=>1,
        'fecha'=>'2020-02-01',
        ]);

        DB::table('historial_estados')->insert([
        'incidencia_id'=> 1,
        'estado_id'=>2,
        'fecha'=>'2020-02-05',
        ]);

        DB::table('historial_estados')->insert([
        'incidencia_id'=> 2,
        'estado_id'=>1,
        'fecha'=>'2020-03-01',
        ]);

        DB::table('historial_estados')->insert([
        'incidencia_id'=> 2,
        'estado_id'=>2,
        'fecha'=>'2020-03-01',
        ]);


        DB::table('historial_estados')->insert([
        'incidencia_id'=> 3,
        'estado_id'=>1,
        'fecha'=>'2020-04-01',
        ]);

        DB::table('historial_estados')->insert([
        'incidencia_id'=> 1,
        'estado_id'=>2,
        'fecha'=>'2020-04-15',
        ]);


    }

    
}
