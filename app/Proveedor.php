<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;


class Proveedor extends Model implements Auditable
{

    use \OwenIt\Auditing\Auditable;

    public $timestamps = true;

    protected $fillable = ['razon_social', 'descripcion', 'telefono', 'email', 'domicilio', 'barrio_id'];

    protected $table = 'proveedores';

    public function barrio(){
        return $this->belongsTo('App\Barrio');
    }

    public function direccion(){
        return $this->domicilio . ", " . $this->barrio->nombre . ", " . $this->barrio->ciudad->nombre . ", " . $this->barrio->ciudad->provincia->nombre . ", " . $this->barrio->ciudad->provincia->pais->nombre;
    }

    public function pais(){
        return $this->barrio->ciudad->provincia->pais->nombre;
    }

    public function provincia(){
        return $this->barrio->ciudad->provincia->nombre;
    }

    public function ciudad(){
        return $this->barrio->ciudad->nombre;
    }




}
