<?php

namespace App;
use App\Barrio;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $fillable = [
        'codigo','nombre', 'cod_postal', 'provincia_id',
    ];

    public $timestamps = false;

    protected $table = 'ciudades';


    public function provincia(){
        return $this->belongsTo('App\Provincia');
    }

    public function barrios(){
        return $this->hasMany('App\Barrio');
    }


    public function existe_referencia()
    {

        $ref = Barrio::whereCiudad_id($this->id)->count();
        //dd($ref);

        if($ref > 0) //existe referencia
        {
            return true;
        }
        else    //no hay referencia
        {
            return false;
        }

    }
}


