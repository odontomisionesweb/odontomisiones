<div class="modal fade modal-slide-in-right"
     aria-hidden="true"
     role="dialog"
     tabindex="-1"
     id="modal-ver-{{$p->id}}">



        <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header" style="background-color:#3490DC">
              <h3 class="modal-title" style="color: white"><i style="color: white" class="fas fa-id-card" aria-hidden="true"></i> Detalle del Proveedor </h3>
              </div>
              <div class="modal-body" style="color: black">
                <ul>
                  <li style="font-size:100%"><b>Razón Social :</b> {{$p->razon_social}} </li>

                  <li style="font-size:100%"><b>Descripción: </b>{{$p->descripcion}} </li>
                  <li style="font-size:100%"><b>Teléfono: </b>
                    
                    @if (is_null($p->telefono)) -
                    @else {{$p->telefono}}
                    @endif


                  </li>
                  <li style="font-size:100%"><b>Correo Electrónico: </b>{{$p->email}} </li>
                  <li style="font-size:100%"><b>Dirección: </b>{{$p->direccion()}} </li>
                  <li style="font-size:100%"><b>Fecha Alta: </b>{{($p->created_at)->format('d/m/Y')}} </li>

                </ul>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>


              </div>
            </div>
        </div>





</div>