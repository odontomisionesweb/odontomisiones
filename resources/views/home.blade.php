@extends('layouts.app')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
@section('content')
<head>
    <style>
        .carousel-inner img {
            width: 370px;
            height: 370px;
        }

        .carousel-inner{
            height: 370px;
        }


    </style>
</head>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                    <div class="h1" align="center"> 
                      OdontoMisiones
                    </div>
                    <div class="h3" align="center"> 
                      Bienvenido {{$u->nombre}}
                    </div>
                    <div class="col text-center font-weight-bold my-4">

                      
                        <div class="row py-2">
                            <div class="col-md-4 pr-4">


                              <a href="{{ route('paises.index') }}" class="card p-3 my-2 border-0 text-dark bg-info">
                                   Países ({{$cantPaises}})
                              </a>

                              <a href="{{ route('provincias.index') }}" class="card p-3 my-2 border-0 text-white bg-dark">
                                   Provincias ({{$cantProvincias}})
                              </a>

                              <a href="{{ route('ciudades.index') }}" class="card p-3 my-2 border-0 text-dark bg-success">
                                   Ciudades ({{$cantCiudades}})
                              </a>

                              <a href="{{ route('barrios.index') }}" class="card p-3 my-2 border-0 text-dark bg-warning">
                                   Barrios ({{$cantBarrios}})
                              </a>
                              <a href="{{ route('proveedores.index') }}" class="card p-3 my-2 border-0 text-dark bg-secondary">
                                   Proveedores ({{$cantProveedores}})
                              </a>
                              <a href="{{ route('users.index') }}" class="card p-3 my-2 border-0 text-white bg-danger">
                                   Usuarios ({{$cantUserAdm+$cantUserOpe+$cantUserTec}})
                              </a>
                            </div>

                        
                            <div class="col pr-4 my-2">

                              <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                  <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                                  <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                                  <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                                </ol>
                                <div class="carousel-inner">
                                  <div class="carousel-item active">
                                    <img src="/img/odonto1.jpg" class="d-block w-100" alt="no hay nada">
                                    <div class="carousel-caption d-none d-md-block text-dark">
                                      <h5> <p style="color:white; font-size:150%; background-color:black"><b>Odonto Misiones</b></p></h5>
                                      <p style="color:white; font-size:150%; background-color:black"><b>Nos avalan las mejores compañías</b></p>
                                    </div>
                                  </div>
                                  <div class="carousel-item">
                                    <img src="/img/odonto2.jpg" class="d-block w-100" alt="...">
                                    <div class="carousel-caption d-none d-md-block text-dark">
                                        <h5> <p style="color:white; font-size:150%; background-color:black"><b>Odonto Misiones</b></p></h5>
                                        <p style="color:white; font-size:150%; background-color:black"><b>Nos avalan las mejores compañías</b></p>                            </div>
                                  </div>
                                  <div class="carousel-item">
                                    <img src="/img/odonto3.jpg" class="d-block w-100" alt="...">
                                    <div class="carousel-caption d-none d-md-block text-dark">
                                        <h5> <p style="color:white; font-size:150%; background-color:black"><b>Odonto Misiones</b></p></h5>
                                        <p style="color:white; font-size:150%; background-color:black"><b>Nos avalan las mejores compañías</b></p>                            </div>
                                  </div>
                                  <div class="carousel-item">
                                    <img src="/img/odonto4.jpg" class="d-block w-100" alt="...">
                                    <div class="carousel-caption d-none d-md-block text-dark">
                                        <h5> <p style="color:white; font-size:150%; background-color:black"><b>Odonto Misiones</b></p></h5>
                                        <p style="color:white; font-size:150%; background-color:black"><b>Nos avalan las mejores compañías</b></p>                            </div>
                                  </div>
                                  <div class="carousel-item">
                                    <img src="/img/odonto5.jpg" class="d-block w-100" alt="...">
                                    <div class="carousel-caption d-none d-md-block text-dark">
                                        <h5> <p style="color:white; font-size:150%; background-color:black"><b>Odonto Misiones</b></p></h5>
                                        <p style="color:white; font-size:150%; background-color:black"><b>Nos avalan las mejores compañías</b></p>                            </div>
                                  </div>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                  <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                  <span class="sr-only">Next</span>
                                </a>
                              </div>

                            </div>
                              

                              
                          </div>

              
                      </div>        
                    </div>
                  </div>
                </div>
                    
              </div>
          </div>
@endsection
