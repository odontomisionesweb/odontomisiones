@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">

                <div class="card-header h3">
                    Crear Provincia
                    <a href="{{route('provincias.index')}}">
                    <button title="volver" class="btn btn-secondary btn-responsive">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Atrás
                    </button>
                </a>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('provincias.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="codigo" class="col-md-4 col-form-label text-md-right">Código</label>

                            <div class="col-md-2">
                                <input id="codigo" type="codigo" class="form-control @error('codigo') is-invalid @enderror" name="codigo" value="{{ old('codigo') }}" autocomplete="codigo" autofocus>

                                @error('codigo')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="nombre" class="col-md-4 col-form-label text-md-right">Nombre</label>

                            <div class="col-md-6">
                                <input id="nombre" type="nombre" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ old('nombre') }}" required autocomplete="nombre" autofocus>

                                @error('nombre')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="pais_id" class="col-md-4 col-form-label text-md-right">País</label>
                            <div class="col-md-6">


                                <select class="custom-select" id="pais_id" name="pais_id" required>
                                    <option selected disabled value="0">Elegir País</option>
                                @foreach($paises as $p)
                                    <option value="{{$p->id}}">{{$p->nombre}}</option>
                                @endforeach
                                </select>
                                <div class="invalid-tooltip">
                                  Por favor seleccione un país valido
                                </div>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Crear
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

