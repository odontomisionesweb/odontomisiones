@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header h3">
                    Editar Usuario
                  <a href="{{route('users.index')}}">
                    <button title="agregar" class="btn btn-secondary btn-responsive">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Atras
                    </button>
                </a>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('users.update', $user) }}">
                        {{ csrf_field() }}
                        {{ method_field('put') }}


                        <div class="form-group row">
                            <label for="nombre" class="col-md-4 col-form-label text-md-right">Nombre</label>
                            <div class="col-md-6">
                                <input id="nombre" type="string" class="form-control @error('codigo') is-invalid @enderror" name="nombre" value="{{old('nombre', $user->nombre)}}">
                                @error('nombre')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="telefono" class="col-md-4 col-form-label text-md-right">Telefono</label>
                            <div class="col-md-6">
                                <input id="telefono" type="string" class="form-control @error('telefono') is-invalid @enderror" name="telefono" value="{{old('telefono', $user->telefono)}}">
                                @error('telefono')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="notas_part" class="col-md-4 col-form-label text-md-right">Notas particulares</label>
                            <div class="col-md-6">
                                <input id="notas_part" type="string" class="form-control @error('notas_part') is-invalid @enderror" name="notas_part" value="{{old('notas_part', $user->notas_part)}}">
                                @error('notas_part')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="apellido" class="col-md-4 col-form-label text-md-right">Apellido</label>
                            <div class="col-md-6">
                                <input id="apellido" type="string" class="form-control @error('apellido') is-invalid @enderror" name="apellido" value="{{old('apellido', $user->apellido)}}">
                                @error('apellido')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">E-mail</label>
                            <div class="col-md-6">
                                <input id="email" type="string" class="form-control @error('email') is-invalid @enderror" name="email" value="{{old('email', $user->email)}}">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Contraseña</label>
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  value="{{old('email', $user->password)}}">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="domicilio" class="col-md-4 col-form-label text-md-right">Domiclio</label>
                            <div class="col-md-6">
                                <input id="domicilio" type="domicilio" class="form-control @error('domicilio') is-invalid @enderror" name="domicilio" value="{{old('domicilio', $user->domicilio)}}">
                                @error('domicilio')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>



                        <div class="form-group row">
                            <label for="barrio_id" class="col-md-4 col-form-label text-md-right">Barrio</label>
                            <div class="col-md-6">


                                <select class="custom-select" id="barrio_id" name="barrio_id" required>
                                    <option selected disabled value="0">Elegir Barrio</option>
                                @foreach($barrios as $barrio)
                                    @if (old('ciudades', $user->barrio_id)== $barrio->id)
                                        <option value="{{$barrio->id}}" selected>
                                            {{$barrio->nombre}}
                                        </option>
                                    @else
                                        <option value="{{$barrio->id}}">
                                            {{$barrio->nombre}}
                                        </option>
                                    @endif
                                @endforeach

                                </select>
                                <div class="invalid-tooltip">
                                  Por favor seleccione un Barrio
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="rol_id" class="col-md-4 col-form-label text-md-right">Rol</label>
                            <div class="col-md-6">


                                <select class="custom-select" id="rol_id" name="rol_id" required>
                                    <option selected disabled value="0">Elegir Rol</option>
                                @foreach($roles as $rol)
                                    @if (old('ciudades', $user->rol_id)== $rol->id)
                                        <option value="{{$rol->id}}" selected>
                                            {{$rol->nombre}}
                                        </option>
                                    @else
                                        <option value="{{$rol->id}}">
                                            {{$rol->nombre}}
                                        </option>
                                    @endif
                                @endforeach

                                </select>
                                <div class="invalid-tooltip">
                                  Por favor seleccione un Rol
                                </div>
                            </div>
                        </div>




                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Editar
                                </button>
                            </div>
                        </div>


                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
