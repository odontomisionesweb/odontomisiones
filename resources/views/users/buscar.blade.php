

    {!! Form::model(Request::only(
        ['rol_id', 'barrio_id']),
        ['url' => 'users/', 'method'=>'GET', 'autocomplete'=>'on', 'role'=>'search']

        )!!}

        <div class="form-group row">
            <label for="rol_id" class="col-md-4 col-form-label text-md-right">Rol</label>
            <div class="col-md-6">
                <select
                    name="rol_id"
                    id="rol_id"
                    class="rol_id custom-select"
                    >
                    @foreach ($roles as $rol)
                        <option
                            value="{{$rol->id}}"
                            @if($rol_id!=null && $rol_id==$rol->id)
                                selected
                            @endif
                        >
                        {{$rol->nombre}}
                        </option>
                    @endforeach
                    <option
                        value="0"
                        @if($rol_id == null || $rol_id==0)
                            selected
                        @endif
                    >
                        -- Todas los roles --
                    </option>
                </select>
            </div>
        </div>


        <div class="form-group row">
            <label for="barrio_id" class="col-md-4 col-form-label text-md-right">Barrio</label>
            <div class="col-md-6">
                <select
                    name="barrio_id"
                    id="barrio_id"
                    class="barrio_id custom-select"
                    >
                    @foreach ($barrios as $barrio)
                        <option
                            value="{{$barrio->id}}"
                            @if($barrio_id!=null && $barrio_id==$barrio->id)
                                selected
                            @endif
                        >
                        {{$barrio->nombre}}
                        </option>
                    @endforeach
                    <option
                        value="0"
                        @if($barrio_id == null || $barrio_id==0)
                            selected
                        @endif
                    >
                        -- Todas los Barrios --
                    </option>
                </select>
            </div>
        </div>




        <div class="form-group row">
            <label for="buscar" class="col-md-4 col-form-label text-md-right"></label>
            <div class="col-md-6">
                <span class="input-group-btn">
                    <button
                        title="buscar"
                        type="submit"
                        id="bt_add"
                        name="filtrar"
                        class="btn btn-primary btn-responsive">
                            <i class="fa fa-filter"></i> Filtrar
                    </button>

                    <a

                    href= "{{ route('users.index') }}"
                    class="btn btn-light"
                    >
                    <i class="fas fa-eraser"></i>
                        ... Limpiar
                </a>

                </span>
            </div>
        </div>






    {{Form::close()}}
<script type="text/javascript">
$(document).ready(function(){


    //si existe un cambio en Fecha Desde
    $('#desde').change(function() {

        //Se captura su valor
        var desde = $(this).val();
        console.log(desde, 'Se cambio la fecha DESDE')
        //y establesco ese valor capturado como minimo en Fecha Hasta
        $('#hasta').attr({"min" : desde});;


        });

    //si existe un cambio en Fecha Hasta
    $('#hasta').change(function() {

        //Se captura su valor
        var hasta = $(this).val();
        console.log(hasta, 'Se cambio la fecha HASTA');

        //si ese valor es diferente a nulo (osea si hay algo dentro)
        if (hasta != "")
        {
            //se deshabilita el desde (para evitar que desde sea mayor que hasta)
            $("#desde").prop('disabled', true);

        }



      });

     //si se clickea en "FIltrar"
      $('#bt_add').click(function () {
        //se debe refrescar (si es que hubo) la prop disabled de desde
        $("#desde").prop('disabled', false);


      });


});







</script>

