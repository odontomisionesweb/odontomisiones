<?php

use Illuminate\Database\Seeder;

class RolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'nombre'=> 'Administrador',
        ]);

        DB::table('roles')->insert([
            'nombre'=> 'Operador',
        ]);

        DB::table('roles')->insert([
            'nombre'=> 'Tecnico',
        ]);

        DB::table('roles')->insert([
            'nombre'=> 'Auditor',
        ]);
    }
}
