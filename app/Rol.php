<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    public $timestamps = false;

    protected $fillable = ['nombre'];

    protected $table = 'roles';
}
