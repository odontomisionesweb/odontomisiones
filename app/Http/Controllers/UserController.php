<?php

namespace App\Http\Controllers;
use App\User;
use App\Pais;
use App\Provincia;
use App\Ciudad;
use App\Barrio;
use App\EstadoCrediticio;

use App\Rol;
use DB;
use App\Incidencia;
use Response;
use Illuminate\Http\Request;


class UserController extends Controller
{

    public function buscarProvincia(Request $request)
	{
	    $provincias=Provincia::select('nombre','id')
			->where('pais_id',$request->id)
            ->get();
        return response()->json($provincias);
	}

    public function buscarCiudad(Request $request)
	{

		$ciudades=Ciudad::select('nombre','id')
			->where('provincia_id',$request->id)
			->get();
		return response()->json($ciudades);

    }

    public function buscarBarrio(Request $request)
	{

		$barrios=Barrio::select('nombre','id')
			->where('ciudad_id',$request->id)
			->get();
		return response()->json($barrios);

	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $users=User::all();
        $roles=Rol::all();
        //$estado_crediticios=EstadoCrediticio::all();
        $barrios=Barrio::all();



        if(count($request->all())>1)
        {
            //dd($request->all());
            $sql = User::select('users.*');

            if($request->rol_id)
            {
                $sql = $sql->whereRol_id($request->rol_id);
            }
            /*if($request->estado_crediticio_id)
            {
                $sql = $sql->whereEstado_crediticio_id($request->estado_crediticio_id);
            }*/
            if($request->barrio_id)
            {
                $sql = $sql->whereBarrio_id($request->barrio_id);
            }


            $users=$sql->orderBy('created_at','desc')->get();
            $rol_id=$request->rol_id;
            //$estado_crediticio_id=$request->estado_crediticio_id;
            $barrio_id=$request->barrio_id;


        }
        else
        {
            $rol_id=null;
            //$estado_crediticio_id=null;
            $barrio_id=null;

            $users=User::orderBy('created_at','desc')->get();


        }

        return view('users.index',[
            "users"                  =>  $users,
            "rol_id"                 =>  $rol_id,
            "roles"                  =>  $roles,
            //"estado_crediticios"     =>  $estado_crediticios,
            //"estado_crediticio_id"   =>  $estado_crediticio_id,
            "barrios"                =>  $barrios,
            "barrio_id"              =>  $barrio_id,


            ]);




    }






    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $paises=Pais::all();
        $provincias=Provincia::all();
        $ciudades=Ciudad::all();
        $barrios=Barrio::all();
        $roles = Rol::all();

        return view('users.create' ,compact('paises','provincias','ciudades', 'barrios','roles'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user=User::create([
            'nombre'      =>    $request->nombre,
            'telefono'    =>    $request->telefono,
            'apellido'    =>    $request->apellido,
            'email'       =>    $request->email,
            'password'    =>    $request->password,
            'domicilio'   =>    $request->domicilio,
            'barrio_id'   =>    $request->barrio_id,
            'rol_id'      =>    $request->rol_id,
            'notas_part' =>     $request->notas_part,


        ]);

        return redirect()->route('users.index')->withMessage('Usuario creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $roles = Rol::all();
        $barrios = Barrio::all();
        return view('users.edit',compact('user', 'roles', 'barrios'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {

        $user->update([
            'nombre'    => $request->nombre,
            'telefono'  => $request->telefono,
            'apellido'  => $request->apellido,
            'email'     => $request->email,
            'password'  => $request->password,
            'domicilio' => $request->domicilio,
            'barrio_id' => $request->barrio_id,
            'rol_id'    => $request->rol_id,
        ]);

        return redirect()->route('users.index')->withMessage('Usuario modificado');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user) {


        /* Este metodo esta en la clase User, lo que hace
            es preguntarse si hay referencia asociada con la clase
            Incidencia, si el tecnico o el usuario tienen incidencia
            dispara el mensaje
        */
        if ($user->existe_referencia()) {
            return redirect()->route('users.index')->withErrors('No se puede eliminar el usuario, hay referencias asociadas');
        }

        $user->delete();
        return redirect()->route('users.index')->with('message','Usuario eliminado');




    }
}
