<?php

use Illuminate\Database\Seeder;

class ProveedorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('proveedores')->insert([
        'razon_social'=> 'Prov1',
        'descripcion'=> 'Descrip Prov1',
        'telefono'=>'3758663117', 
        'email'=>'prov1@mail.com',
        'domicilio'=>'9 de Julio',
        'barrio_id'=>2,
        'created_at'=>'2020-07-01',
        ]);

        DB::table('proveedores')->insert([
        'razon_social'=> 'Prov2',
        'descripcion'=> 'Descrip Prov2',
        'telefono'=>'3758669475', 
        'email'=>'prov2@mail.com',
        'domicilio'=>'Av Roque Perez',
        'barrio_id'=>1,
        'created_at'=>'2020-08-01',
        ]);

        DB::table('proveedores')->insert([
        'razon_social'=> 'Prov3',
        'descripcion'=> 'Descrip Prov3',
        'telefono'=>'3758844562', 
        'email'=>'prov3@mail.com',
        'domicilio'=>'Belgrano 305',
        'barrio_id'=>2,
        'created_at'=>'2020-07-01',
        ]);


    }
}
