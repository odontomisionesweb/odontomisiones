<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\BarrioRequest;
use App\Barrio;
use Illuminate\Validation\Rule;
use App\Ciudad;

class BarrioController extends Controller
{
    public function index(Request $request)
    {
        
        $barrios=Barrio::all();
        $ciudades=Ciudad::all();

        /*$desde=$request->desde;
        $hasta=$request->hasta;*/

        if(count($request->all())>1)
        {
            //dd($request->all());
            $sql = Barrio::select('barrios.*');

            if($request->ciudad_id)
            {
                $sql = $sql->whereciudad_id($request->ciudad_id);
            }
            /*if($request->desde)
            {
                $sql = $sql->whereDate('created_at','>=',$request->desde);
            }
            if($request->hasta)
            {
                $sql = $sql->whereDate('created_at','<=',$request->hasta);
            }
            */

            $barrios=$sql->orderBy('created_at','desc')->get();
            $ciudad_id=$request->ciudad_id;


        }
        else
        {
            $ciudad_id=null;

            $barrios=Barrio::orderBy('created_at','desc')->get();


        }

        return view('barrios.index',[
            "barrios"         =>  $barrios,
            "ciudad_id"        =>  $ciudad_id,
            "ciudades"         =>  $ciudades,
            /*"desde"         =>  $desde,
            "hasta"         =>  $hasta,*/

        ]);


      
    }

    public function create()
    {
    	$ciudades=Ciudad::all();
    	return view('barrios.create', compact('ciudades'));
    }

    public function store(BarrioRequest $request, Barrio $barrio)
    {
        $request->validate([
            'codigo'=>[ Rule::unique('barrios'),
                        'required', 'regex:/^[a-zA-Z]+$/u', 'max:5' ],


            //'codigo' => 'unique:paises|required|max:5|regex:/^[a-zA-Z]+$/u',//regex sin espacios
            'nombre'=>'required|regex:/^[a-zA-Z][a-zA-Z ]+$/u|max:50',//regex con espacios
        ], [
            'required' => 'Campo requerido',
            'codigo.unique' => 'Está ingresando un código existente',
            'regex' => 'Complete el campo únicamente con letras',
            //no toma
            'codigo.max' => 'Se permiten hasta 5 caracteres',
            'nombre.max' => 'Se permiten hasta 50 caracteres',
        ]);
        $barrio=Barrio::create([
            'codigo'=>$request->codigo,
            'nombre'=>$request->nombre,
            'ciudad_id'=>$request->ciudad_id,
        ]);

        return redirect()->route('barrios.index')->withMessage('Barrio creado');
    }

    public function edit(Barrio $barrio)
    {
        $ciudades=Ciudad::all();
        return view('barrios.edit',compact('barrio', 'ciudades'));
    }

    public function update(BarrioRequest $request, Barrio $barrio)
    {
        $request->validate([
            'codigo'=>[ Rule::unique('barrios')->ignore($barrio),
                        'required', 'regex:/^[a-zA-Z]+$/u', 'max:5' ],


            //'codigo' => 'unique:paises|required|max:5|regex:/^[a-zA-Z]+$/u',//regex sin espacios
            'nombre'=>'required|regex:/^[a-zA-Z][a-zA-Z ]+$/u|max:50',//regex con espacios
        ], [
            'required' => 'Campo requerido',
            'codigo.unique' => 'Está ingresando un código existente',
            'regex' => 'Complete el campo unicamente con letras',
            //no toma
            'codigo.max' => 'Se permiten hasta 5 caracteres',
            'nombre.max' => 'Se permiten hasta 50 caracteres',
        ]);

        $barrio->update([
            'codigo'=>$request->codigo,
            'nombre' => $request->nombre,
            'ciudad_id' =>$request->ciudad_id,
        ]);

        return redirect()->route('barrios.index')->withMessage('Barrio modificado');
    }

    public function destroy(Barrio $barrio) {

        //dd("destroy");

        if ($barrio->existe_referencia()) {
            return redirect()->route('barrios.index')->withErrors('No se puede eliminar el barrio, hay referencias asociadas');
        }

        $barrio->delete();
        return redirect()->route('barrios.index')->with('message','Barrio eliminado');




    }
}
