<?php

use Illuminate\Database\Seeder;

class EstadoCrediticioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('estado_crediticios')->insert([
            'nombre'=> 'Fiable',
            ]);

        DB::table('estado_crediticios')->insert([
            'nombre'=> 'No Fiable',
            ]);
    }
}
