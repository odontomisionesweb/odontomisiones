@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">

                <div class="card-header h3">
                    Ciudades
                  <a href="{{route('ciudades.create')}}">
                    <button title="agregar ciudad" class="btn btn-success btn-responsive">
                        <i class="fa fa-plus"></i> Nuevo
                    </button>
                </a>
                <a href="{{route('home')}}">
                    <button title="volver" class="btn btn-secondary btn-responsive">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Atrás
                    </button>
                </a>
                </div>
                <div class="card-body">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="card collapsed-card">

                            <div class="card-body">

                                @include('ciudades.buscar') <!-- elemento de la -->



                            </div>

                        </div>
                        <br><!-- salteamos un renglon -->
                        @include('layouts.mensaje')
                    
                    <table id="tabla" class="table table-bordered table-hover border-dark">
                        <thead class="thead-dark">
                           
                                   
                            <tr>
                                <th scope="col">Código</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">País</th>
                                <th scope="col">Provincia</th>
                                <th scope="col">Código Postal</th>
                                <th scope="col">Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($ciudades as $ciudad)
                                <tr>
                                    <td>{{ $ciudad->codigo }}</td>
                                    <td>{{ $ciudad->nombre }}</td>
                                    <td>{{ $ciudad->Provincia->pais->nombre }}</td>
                                    <td>{{ $ciudad->Provincia->nombre }}</td>
                                    <td>{{ $ciudad->cod_postal }}</td>
                                    
                                        <td>
                                
                                            <a href="{{route('ciudades.edit', $ciudad)}}" type="button" class="btn btn-light btn-sm"><i class="fa fa-edit"></i> Editar</a>
                                        
                                            <a data-keyboard="false" data-target="#modal-delete-{{$ciudad->id}}" data-toggle="modal">
                                                    <button title="eliminar" class="btn btn-danger btn-responsive">
                                                        <i class="fa fa-trash"></i> Eliminar
                                                    </button>
                                            </a>
                                        </td>
                                </tr>
                                @include('ciudades.delete')
                            @endforeach
                        </tbody>
                    </table>
            
                </div>
              </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#tabla').DataTable({
              "aaSorting": [],
              "pageLength" : 5,
              "lengthMenu": [[5, 10], [5, 10]],
              language: {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",

                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }

                }

            });




        } );
    </script>
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@endsection

