<div class="modal fade modal-slide-in-right"
     aria-hidden="true"
     role="dialog"
     tabindex="-1"
     id="modal-ver-{{$user->id}}">



        <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header" style="background-color:#3490DC">
              <h3 class="modal-title" style="color: white"><i style="color: white" class="fas fa-id-card" aria-hidden="true"></i> Detalle del Usuario </h3>
              </div>
              <div class="modal-body" style="color: black">
                <ul>
                  <li style="font-size:100%"><b>Apellidos y Nombres :</b> {{$user->apellido}} {{$user->nombre}}</li>

                  <li style="font-size:100%"><b>Telefono: </b>{{$user->telefono}} </li>
                  <li style="font-size:100%"><b>Correo Electronico: </b>{{$user->email}} </li>
                  <li style="font-size:100%"><b>Direccion: </b>{{$user->direccion()}} </li>
                  <li style="font-size:100%"><b>Rol: </b>{{$user->rol->nombre}} </li>
                  <li style="font-size:100%"><b>Notas Particulares : </b>{{$user->notas_part}} </li>

                </ul>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>


              </div>
            </div>
        </div>





</div>

