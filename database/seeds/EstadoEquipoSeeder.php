<?php

use Illuminate\Database\Seeder;

class EstadoEquipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('estados_equipos')->insert([
        'nombre'=> 'Nuevo',
        ]);

        DB::table('estados_equipos')->insert([
        'nombre'=> 'Usado',
        ]);
    }
}
